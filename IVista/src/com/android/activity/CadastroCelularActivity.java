package com.android.activity;

import java.io.IOException;

import org.apache.http.client.ClientProtocolException;
import org.json.JSONException;
import org.json.JSONObject;

import com.android.functions.GetSetJSON;
import com.android.functions.VariaveisGlobais;
import com.android.projetoavista1.R;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.EditText;
import android.widget.TextView;

public class CadastroCelularActivity extends FragmentActivity {
	public String celularInformar;
	public String celularConfirmar;
	public String numeroCelular;
	public String openURL = "http://177.125.181.172/BossRest/nao/cliente/cadastrar/celular/";
	public GetSetJSON getSetJson = new GetSetJSON();
	String codigoValidacao;
	boolean dadosEnviados = false;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.activity_cadastro_celular);

		if (savedInstanceState == null) {
			setContentView(R.layout.activity_cadastro_celular);

			TextView txTitulo = (TextView) findViewById(R.id.txtMeuCelular);
			TextView txDesc = (TextView) findViewById(R.id.descricaoCelular);

			mudarFonte(txTitulo, "fonts/Proxima_Nova_Regular.otf");
			mudarFonte(txDesc, "fonts/Proxima_Nova_Regular.otf");

		}
		FrgCelularInformar frgCelularInformar = new FrgCelularInformar();
		FragmentManager fManager = getSupportFragmentManager();
		FragmentTransaction fTransaction = fManager.beginTransaction();
		fTransaction.add(R.id.frlConfirmacao, frgCelularInformar);
		fTransaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
		fTransaction.commit();
		if (VariaveisGlobais.mostrarMensagem == true) {
			new AlertDialog.Builder(CadastroCelularActivity.this)
					.setTitle("Importante")
					.setMessage(
							"O n�mero do celular que voc� vai informar precisa estar em suas m�os. "
									+ "Pois voc� receber� um SMS com o c�digo de seguran�a para iniciar o seu cadastro.")
					.setNeutralButton("OK", null).show();
		}
	}

	@Override
	public void onBackPressed() {
		if (dadosEnviados == true) {
			dialogFechar();
		} else {
			VariaveisGlobais.mostrarMensagem = true;
			this.finish();
		}
	}

	private void mudarFonte(TextView tv, String fonte) {

		Typeface type = Typeface.createFromAsset(getAssets(), fonte);

		tv.setTypeface(type);

	}

	public void onClickBtnCancelar(View v) {
		if (dadosEnviados == true) {
			dialogFechar();
		} else {
			VariaveisGlobais.mostrarMensagem = true;
			this.finish();
		}
	}

	public void dialogFechar() {
		new AlertDialog.Builder(CadastroCelularActivity.this)
				.setTitle("Deseja Cancelar?")
				.setMessage(
						"Ao cancelar o processo este n�mero de celular n�o poder� ser ultilizado em uma nova proposta.")
				.setNegativeButton("N�o", new OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						dialog.dismiss();
					}
				}).setPositiveButton("Sim", new OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						finish();
					}
				}).show();
	}

	// Botão de confirmação do primeiro fragment:
	public void celularInformarBtnOk(View v) {
		com.android.functions.EditTextCelular edtCelularInfo = (com.android.functions.EditTextCelular) findViewById(R.id.edtCelularInfo);
		if (edtCelularInfo.getText().toString().equals("")) {
			new AlertDialog.Builder(CadastroCelularActivity.this)
					.setTitle("Importante")
					.setMessage("Favor inserir um n�mero de celular v�lido.")
					.setNeutralButton("OK", null).show();
		} else if (edtCelularInfo.length() < 14) {
			new AlertDialog.Builder(CadastroCelularActivity.this)
					.setTitle("Importante")
					.setMessage(
							"Favor inserir um n�mero de celular com nove d�gitos")
					.setNeutralButton("OK", null).show();
		} else {
			celularInformar = edtCelularInfo.getText().toString();// Guarda o
																	// primeiro
																	// numero;
			FrgCelularConfirmar frgCelularConfirmar = new FrgCelularConfirmar();
			FragmentManager fManager = getSupportFragmentManager();
			FragmentTransaction fTransaction = fManager.beginTransaction();
			fTransaction.replace(R.id.frlConfirmacao, frgCelularConfirmar);
			fTransaction
					.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
			fTransaction.commit();
		}
	}

	// Botão de confirmação do segundo framgment:
	public void celularConfirmarBtnOk(View v) {
		com.android.functions.EditTextCelular edtCelularConf = (com.android.functions.EditTextCelular) findViewById(R.id.edtCelularConf);
		if (edtCelularConf.getText().toString().equals("")) {
			new AlertDialog.Builder(CadastroCelularActivity.this)
					.setTitle("Importante")
					.setMessage("Favor inserir um n�mero de celular v�lido!")
					.setNeutralButton("OK", null).show();
		} else if (edtCelularConf.length() < 14) {
			new AlertDialog.Builder(CadastroCelularActivity.this)
					.setTitle("Importante")
					.setMessage(
							"Favor inserir um n�mero de celular com nove d�gitos")
					.setNeutralButton("OK", null).show();
		} else {
			celularConfirmar = edtCelularConf.getText().toString();
			if (celularInformar.equals(celularConfirmar)) {
				numeroCelular = celularConfirmar;
				// Manda os dados para o webserice:
				new CadastroCelular().execute(numeroCelular);
			} else {
				new AlertDialog.Builder(CadastroCelularActivity.this)
						.setTitle("Aten��o")
						.setMessage("Os n�meros informados n�o conferem.")
						.setPositiveButton("OK", new OnClickListener() {
							@Override
							public void onClick(DialogInterface dialog,
									int which) {
								VariaveisGlobais.mostrarMensagem = false;
								Intent intent = new Intent(
										CadastroCelularActivity.this,
										CadastroCelularActivity.class);
								startActivity(intent);
								CadastroCelularActivity.this.finish();
							}
						}).show();
			}
		}
	}

	// Botão de confirmação do terceiro fragment:
	public void celularCodSegurancaContinuar(View v) {
		EditText edtCodSeguranca = (EditText) findViewById(R.id.edtCodSeguranca);
		Log.i("Codigo Valida��o", codigoValidacao);
		if (edtCodSeguranca.getText().toString().equals("")) {
			new AlertDialog.Builder(CadastroCelularActivity.this)
					.setTitle("Aten��o")
					.setMessage("Informe o c�digo enviado via SMS.")
					.setNeutralButton("Fechar", null).show();
		} else if (!edtCodSeguranca.getText().toString()
				.equals(codigoValidacao)) {
			new AlertDialog.Builder(CadastroCelularActivity.this)
					.setTitle("Aten��o")
					.setMessage(
							"Codigo informado n�o confere com o enviado. Tente novamente.")
					.setNeutralButton("Fechar", null).show();
			edtCodSeguranca.setText("");
		} else {
			Intent intent = new Intent(this, DadosPessoais.class);
			startActivity(intent);
			this.finish();
		}
	}

	public class CadastroCelular extends AsyncTask<String, Void, Void> {
		JSONObject jsonEnvio = new JSONObject();
		JSONObject jsonResposta1;
		int jsonRetorno = 1;
		String mensagemModal;
		ProgressDialog pDialog;
		boolean semConexao = true;

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			pDialog = new ProgressDialog(CadastroCelularActivity.this);
			pDialog.setMessage("Aguarde um instante...");
			pDialog.setIndeterminate(false);
			pDialog.setCancelable(false);
			pDialog.show();
		}

		@Override
		protected Void doInBackground(String... params) {
			final String celular = params[0];
			try {
				jsonEnvio.put("PARAM_NUMERO_CELULAR", celular);
				jsonResposta1 = getSetJson
						.enviarSolicitacao(openURL, jsonEnvio);// Enviando a
				Log.i("json", jsonResposta1.toString());
				try {
					semConexao = false;
					// Pegando a mensagem do modal:
					jsonRetorno = jsonResposta1.getInt("RETORNO");
					mensagemModal = jsonResposta1.getString("MENSAGEM");
					// Pegando o codigo:
					JSONObject jsonResposta2 = jsonResposta1
							.getJSONObject("DADOS");
					codigoValidacao = jsonResposta2.getString("CODIGO");
				} catch (JSONException e) {
					Log.i("Erro Mesagem: ", e.getMessage());
				}
			} catch (JSONException e) {
				Log.i("JSONException", e.getMessage());
				jsonRetorno = 1;
			} catch (ClientProtocolException e) {
				Log.i("ClientProtocolException", e.getMessage());
				jsonRetorno = 1;
			} catch (IOException e) {
				Log.i("IOException", e.getMessage());
				jsonRetorno = 1;
				semConexao = true;
			}
			return null;
		}

		@Override
		protected void onPostExecute(Void Void) {
			pDialog.dismiss();// Fechando a progressBar;
			if (jsonRetorno == 0) {
				if (semConexao == false) {
					new AlertDialog.Builder(CadastroCelularActivity.this)
							.setTitle("Aten��o")
							.setMessage(
									mensagemModal.toString() + "\n\nCodigo: "
											+ codigoValidacao)
							.setNeutralButton("OK", null).show();
					dadosEnviados = true;
					// Instancia o fragment do codigo:
					FrgCodSeguranca FrgCodSeguranca = new FrgCodSeguranca();
					FragmentManager fManager = getSupportFragmentManager();
					FragmentTransaction fTransaction = fManager
							.beginTransaction();
					fTransaction.replace(R.id.frlCodSeguranca, FrgCodSeguranca);
					fTransaction
							.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
					fTransaction.commit();
					EditText edtCelularConf = (EditText) findViewById(R.id.edtCelularConf);
					VariaveisGlobais.contatoCelular = edtCelularConf.getText()
							.toString();
				}
			} else {
				if (semConexao == false) {
					new AlertDialog.Builder(CadastroCelularActivity.this)
							.setTitle("Aten��o").setMessage(mensagemModal)
							.setNeutralButton("OK", null).show();
				} else {
					new AlertDialog.Builder(CadastroCelularActivity.this)
							.setTitle("Aten��o")
							.setMessage("Sem conex�o de rede.")
							.setNeutralButton("OK", null).show();
				}
			}
		}
	}
}
