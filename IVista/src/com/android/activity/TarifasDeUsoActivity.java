package com.android.activity;

import com.android.functions.VariaveisGlobais;
import com.android.projetoavista1.R;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.TextView;

public class TarifasDeUsoActivity extends Activity{
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.activity_tarifas_de_uso);
		
		TextView txtTermosUso = (TextView)findViewById(R.id.txtTarifasUso);
		txtTermosUso.setText(VariaveisGlobais.aprovacaoTarifa);
	}
	
	public void concordarTarifas(View v){
		this.finish();
	}

}
