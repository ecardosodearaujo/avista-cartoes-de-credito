package com.android.activity;

import com.android.functions.VariaveisGlobais;
import com.android.projetoavista1.R;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.TextView;

public class CartaoPreAprovado extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.activity_cred_aprovado);

		TextView endcli = (TextView) findViewById(R.id.endCliente);
		TextView endcli2 = (TextView) findViewById(R.id.endCliente2);

		endcli.setText(VariaveisGlobais.enderecoLogradouro + ", "
				+ VariaveisGlobais.enderecoNumero);
		endcli2.setText(VariaveisGlobais.enderecoBairro + ", "
				+ VariaveisGlobais.enderecoCidade + ", "
				+ VariaveisGlobais.enderecoUf);

	}

	public void onClickBtnCont1(View v) {
		Intent intent = new Intent(this, MinhasFotosActivity.class);
		startActivity(intent);
		this.finish();
	}

	public void onClickFecharWindow(View v) {
		Intent intent = new Intent(CartaoPreAprovado.this,
				AreaClienteActivity.class);
		startActivity(intent);
		this.finish();
	}

	public void onClickCredAprovContinuar(View v) {
		Intent intent = new Intent(CartaoPreAprovado.this,
				AreaClienteActivity.class);
		startActivity(intent);
		this.finish();
	}
}
