package com.android.activity;

import java.io.ByteArrayOutputStream;

import com.android.functions.Base64;
import com.android.functions.VariaveisGlobais;
import com.android.projetoavista1.R;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.DialogInterface.OnClickListener;
import android.graphics.Bitmap;
import android.hardware.Camera;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;

public class MinhasFotosActivity extends Activity {
	private static final String TAG = null;

	public Uri uri1;
	public View view;
	public boolean sMinhaFoto = false;
	public boolean sDocFront = false;
	public boolean sDocEnd = false;
	// public static final int CAMERA_FACING_FRONT;

	private Camera camera;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.activity_minhas_fotos);

		new AlertDialog.Builder(MinhasFotosActivity.this)
				.setTitle("Importante")
				.setMessage(
						"Tire as fotos em um local iluminado. Fotografe a frente e o verso de seu documento (RG ou CNH). "
								+ "� importante que suas fotos estejam legiveis.")
				.setNeutralButton("OK", null).show();

	}

	@Override
	public void onBackPressed() {
		dialogFechar();
	}

	public void onClickBtnCancelar(View v) {
		dialogFechar();
	}

	public void dialogFechar() {
		new AlertDialog.Builder(MinhasFotosActivity.this)
				.setTitle("Deseja Cancelar?")
				.setMessage(
						"Ao cancelar o processo este n�mero de celular n�o poder� ser ultilizado em uma nova proposta.")
				.setNegativeButton("N�o", new OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						dialog.dismiss();
					}
				}).setPositiveButton("Sim", new OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						finish();
					}
				}).show();
	}

	public void onClickBtnCont(View v) {
		if (sMinhaFoto == false) {
			new AlertDialog.Builder(MinhasFotosActivity.this)
					.setTitle("Aten��o")
					.setMessage("� necess�rio tirar a sua foto.")
					.setNeutralButton("OK", null).show();
		} else if (sDocFront == false) {
			new AlertDialog.Builder(MinhasFotosActivity.this)
					.setTitle("Aten��o")
					.setMessage(
							"� necess�rioo tirar a foto da frente do documento.")
					.setNeutralButton("OK", null).show();
		} else if (sDocEnd == false) {
			new AlertDialog.Builder(MinhasFotosActivity.this)
					.setTitle("Aten��o")
					.setMessage(
							"� necess�rio tirar a foto do verso do documento.")
					.setNeutralButton("OK", null).show();
		} else {
			Intent intent = new Intent(this, AssinaturaActivity.class);
			startActivity(intent);
			this.finish();
		}
	}

	public void tirarFoto(View view) {
		MinhasFotosActivity.this.view = view;
		Intent it = new Intent("android.media.action.IMAGE_CAPTURE");
		startActivityForResult(it, 0);
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (data != null) {
			Bundle bundle = data.getExtras();
			if (bundle != null) {
				Bitmap img = (Bitmap) bundle.get("data");
				switch (view.getId()) {
				case (R.id.lltMinhaFoto): {
					// Setando a imagem na tela:
					ImageView minhaFoto = (ImageView) findViewById(R.id.imgMinhaFoto);
					
					minhaFoto.setScaleType(ScaleType.CENTER_CROP);
					minhaFoto.setImageBitmap(img);
					ByteArrayOutputStream stream = new ByteArrayOutputStream();
					img.compress(Bitmap.CompressFormat.JPEG, 100, stream);
					byte[] byteArray = stream.toByteArray();// Transformado a
															// imagem em array
															// de bytes;
					VariaveisGlobais.imagemDocFrontB64 = Base64
							.encodeBytes(byteArray);// Guarda a imagem na
													// variavel global;
					sDocFront = true;
					Log.i("Doc Front", VariaveisGlobais.imagemDocFrontB64);
					break;
				}
				case (R.id.lltDocFront): {
					// Setando a imagem na tela:
					ImageView docFront = (ImageView) findViewById(R.id.imgDocFront);
					docFront.getScaleType();
					docFront.setScaleType(ScaleType.CENTER_CROP);
					docFront.setImageBitmap(img);
					ByteArrayOutputStream stream = new ByteArrayOutputStream();
					img.compress(Bitmap.CompressFormat.JPEG, 100, stream);
					byte[] byteArray = stream.toByteArray();// Transformado a
															// imagem em array
															// de bytes;
					VariaveisGlobais.imagemDocFrontB64 = Base64
							.encodeBytes(byteArray);// Guarda a imagem na
													// variavel global;
					sDocFront = true;
					Log.i("Doc Front", VariaveisGlobais.imagemDocFrontB64);
					break;
				}
				case (R.id.lltDocVerso): {
					// Setando a imagem na tela:
					ImageView docVerso = (ImageView) findViewById(R.id.imgDocVerso);
					docVerso.getScaleType();
					docVerso.setScaleType(ScaleType.CENTER_CROP);
					docVerso.setImageBitmap(img);
					ByteArrayOutputStream stream = new ByteArrayOutputStream();// Variavel
																				// que
																				// recebe
																				// a
																				// imagem
																				// em
																				// bytes;
					img.compress(Bitmap.CompressFormat.JPEG, 100, stream);
					byte[] byteArray = stream.toByteArray();// Transformado a
															// imagem em array
															// de bytes;
					VariaveisGlobais.imagemDocEndB64 = Base64
							.encodeBytes(byteArray);// Guarda a imagem na
													// variavel global;
					sDocEnd = true;
					Log.i("Doc End:", VariaveisGlobais.imagemDocEndB64);
					break;
				}
				}
			}
		}
	}
}
