package com.android.activity;

import com.android.projetoavista1.R;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.DialogInterface.OnClickListener;
import android.os.Bundle;
import android.view.View;
import android.view.Window;

public class PropostaAprovada extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.proposta_aprovada);
	}

	public void onClickSelecionarOpcao(View view) {

		switch (view.getId()) {

		case R.id.bntReceberCartao:
			Intent intent1 = new Intent(getBaseContext(),
					CartaoPreAprovado.class);
			startActivity(intent1);
			PropostaAprovada.this.finish();
			break;

		case R.id.bntImprimir:
			Intent intent2 = new Intent(getBaseContext(),
					ListaDeLojasActivity.class);
			startActivity(intent2);
			PropostaAprovada.this.finish();
			break;

		case R.id.bntVincular:
			Intent intent3 = new Intent(getBaseContext(),
					VincularCartaoActivity.class);
			startActivity(intent3);
			PropostaAprovada.this.finish();
			break;
		}
	}

	@Override
	public void onBackPressed() {
		new AlertDialog.Builder(PropostaAprovada.this)
				.setTitle("Deseja Cancelar?")
				.setMessage(
						"Ao cancelar o processo este n�mero de celular n�o poder� ser ultilizado em uma nova proposta.")
				.setNegativeButton("N�o", new OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						dialog.dismiss();
					}
				}).setPositiveButton("Sim", new OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						finish();
					}
				}).show();
	}
}
