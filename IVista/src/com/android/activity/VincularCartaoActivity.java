package com.android.activity;

import java.io.IOException;
import org.apache.http.client.ClientProtocolException;
import org.json.JSONException;
import org.json.JSONObject;

import com.android.functions.GetSetJSON;
import com.android.functions.Mascaras;
import com.android.projetoavista1.R;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.EditText;

public class VincularCartaoActivity extends Activity {
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.activity_vincular_cartao);

		EditText numerocartao = (EditText) findViewById(R.id.edtNumeroCartao);
		numerocartao.addTextChangedListener(Mascaras.insert("####.####.####.####", numerocartao));
		EditText celularvinc = (EditText) findViewById(R.id.edtCelularVincular);
		celularvinc.addTextChangedListener(Mascaras.insert("(##) ####-####",celularvinc));
		EditText cpfvinc = (EditText) findViewById(R.id.edtCpf1);
		cpfvinc.addTextChangedListener(Mascaras.insert("###.###.###-##",cpfvinc));
		EditText cep = (EditText) findViewById(R.id.edtCep1);
		cep.addTextChangedListener(Mascaras.insert("##.###-###", cep));
	}
	
	public void confirmVinculo(View v){
		new VincularCartao().execute();
	}
	
	public class VincularCartao extends AsyncTask<Void,Void,Void>{
		public ProgressDialog pDialog;
		public JSONObject jsonEnvio = new JSONObject();
		public JSONObject jsonResposta1;
		public int jsonSucesso = 1;
		public int jsonRetorno = 1;
		public String jsonDataNas;
		public String jsonNome;
		public String jsonMensagem;
		public boolean semConexao = true;
		public GetSetJSON getSetJson = new GetSetJSON();
		public String openURL ="http://177.125.181.172/BossRest/nao/cliente/vincular/temporario/";
		
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			pDialog = new ProgressDialog(VincularCartaoActivity.this);
			pDialog.setMessage("Aguarde um instante...");
			pDialog.setIndeterminate(false);
			pDialog.setCancelable(false);
			pDialog.show();
		}
		
		@Override
		protected Void doInBackground(Void... params) {
			EditText numerocartao = (EditText) findViewById(R.id.edtNumeroCartao);
			EditText celularvinc = (EditText) findViewById(R.id.edtCelularVincular);
			EditText cpfvinc = (EditText) findViewById(R.id.edtCpf1);
			EditText cep = (EditText) findViewById(R.id.edtCep1);
			try {
				semConexao=false;
				jsonEnvio.put("PARAM_NUMERO_PLASTICO",numerocartao.getText().toString());
				jsonEnvio.put("PARAM_NUMERO_CELULAR", celularvinc.getText().toString());
				jsonEnvio.put("PARAM_NUMERO_CPF", cpfvinc.getText().toString());
				jsonEnvio.put("PARAM_NUMERO_CEP", cep.getText().toString());
				jsonResposta1=getSetJson.enviarSolicitacao(openURL,jsonEnvio);
				Log.i("Log", jsonResposta1.toString());
				jsonMensagem=jsonResposta1.getString("MENSAGEM");
			} catch (JSONException e) {
				Log.i("JSONException", e.getMessage());
			} catch (ClientProtocolException e) {
				Log.i("ClientProtocolException", e.getMessage());
			} catch (IOException e) {
				Log.i("IOException", e.getMessage());
				semConexao=true;
			}
			return null;
		}
		
		@Override
		protected void onPostExecute(Void Void) {
			pDialog.dismiss();
			if (semConexao==false){
				new AlertDialog.Builder(VincularCartaoActivity.this).setMessage(jsonMensagem)
				.setPositiveButton("Finalizar", new OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						Intent intent = new Intent(VincularCartaoActivity.this,AreaClienteActivity.class);
						startActivity(intent);
						VincularCartaoActivity.this.finish();
					}
				}).show();
			}else{
				new AlertDialog.Builder(VincularCartaoActivity.this).setTitle("Aten��o").setMessage("Sem conex�o de rede.").setNeutralButton("OK", null).show();
			}
		}
	}
}
