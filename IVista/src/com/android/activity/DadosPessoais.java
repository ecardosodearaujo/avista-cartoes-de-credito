package com.android.activity;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import org.apache.http.client.ClientProtocolException;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import com.android.functions.GetSetJSON;
import com.android.functions.Mascaras;
import com.android.functions.VariaveisGlobais;
import com.android.projetoavista1.R;
import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.DialogInterface.OnClickListener;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.text.InputFilter;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.EditText;

public class DadosPessoais extends FragmentActivity {
	public GetSetJSON getSetJson = new GetSetJSON();
	public String openURL = "http://177.125.181.172/BossRest/nao/cliente/consultar/cpf/";

	@SuppressLint("CutPasteId")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.activity_dados_pessoais);

		EditText cpf = (EditText) findViewById(R.id.edtDadosCpf);
		cpf.addTextChangedListener(Mascaras.insert("###.###.###-##", cpf));

		if (VariaveisGlobais.voltarDadosPessoais == true) {
			FrgDadosPessoais frgDadosPessoais = new FrgDadosPessoais();
			FragmentManager fManager = getSupportFragmentManager();
			FragmentTransaction fTransaction = fManager.beginTransaction();
			fTransaction.replace(R.id.frlDadosPessoais, frgDadosPessoais);
			fTransaction
					.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
			fTransaction.commit();

			EditText edtDadosCpf = (EditText) findViewById(R.id.edtDadosCpf);
			edtDadosCpf.setText(VariaveisGlobais.contatoCpf);
		}
	}

	@Override
	public void onBackPressed() {
		dialogFechar();
	}

	public void onClickDadosPessoaisBtnOk(View v) {
		EditText edtDadosCpf = (EditText) findViewById(R.id.edtDadosCpf);
		if (edtDadosCpf.getText().toString().equals("")) {
			new AlertDialog.Builder(DadosPessoais.this).setTitle("Aten��o")
					.setMessage("Informe o CPF.").setNeutralButton("OK", null)
					.show();
		} else if (edtDadosCpf.length() < 14) {
			new AlertDialog.Builder(DadosPessoais.this).setTitle("Aten��o")
					.setMessage("Informe um CPF v�ido.")
					.setNeutralButton("OK", null).show();
		} else {
			new ConsultaCpf().execute(edtDadosCpf.getText().toString());
		}
	}

	public void onClickBtnCancelar(View v) {
		dialogFechar();
	}

	public void dialogFechar() {
		new AlertDialog.Builder(DadosPessoais.this)
				.setTitle("Deseja Cancelar?")
				.setMessage(
						"Ao cancelar o processo este n�mero de celular n�o poder� ser ultilizado em uma nova proposta.")
				.setNegativeButton("N�o", new OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						dialog.dismiss();
					}
				}).setPositiveButton("Sim", new OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						finish();
					}
				}).show();
	}

	public void onClickDadosPessoaisBtnContinuar(View v) {
		EditText edtDadosCnpj = (EditText) findViewById(R.id.edtDadosCpf);
		EditText edtDadosNome = (EditText) findViewById(R.id.edtDadosNome);
		edtDadosNome
				.setFilters(new InputFilter[] { new InputFilter.AllCaps() });
		EditText edtDadosDatNasc = (EditText) findViewById(R.id.edtDadosDatNasc1);
		EditText edtDadosTelAdicional = (EditText) findViewById(R.id.edtDadosTelAdicional);
		com.android.functions.EditTextMoeda edtDadosRenda = (com.android.functions.EditTextMoeda) findViewById(R.id.edtDadosRenda);

		if (edtDadosCnpj.getText().toString().equals("")) {
			new AlertDialog.Builder(DadosPessoais.this).setTitle("Aten��o")
					.setMessage("Informe o CPF.").setNeutralButton("OK", null)
					.show();
		} else if (edtDadosNome.getText().toString().equals("")) {
			new AlertDialog.Builder(DadosPessoais.this).setTitle("Aten��o")
					.setMessage("Informe o nome completo.")
					.setNeutralButton("OK", null).show();
		} else if (edtDadosDatNasc.getText().toString().equals("")) {
			new AlertDialog.Builder(DadosPessoais.this).setTitle("Aten��o")
					.setMessage("Informe a data de nascimento.")
					.setNeutralButton("OK", null).show();
		} else if (edtDadosDatNasc.length() < 10) {
			new AlertDialog.Builder(DadosPessoais.this).setTitle("Aten��o")
					.setMessage("Informe uma data de nascimento v�lida.")
					.setNeutralButton("OK", null).show();
		} else if (edtDadosTelAdicional.getText().toString().equals("")) {
			new AlertDialog.Builder(DadosPessoais.this).setTitle("Aten��o")
					.setMessage("Informe o telefone adicional.")
					.setNeutralButton("OK", null).show();
		} else if (edtDadosTelAdicional.length() < 10) {
			new AlertDialog.Builder(DadosPessoais.this).setTitle("Aten��o")
					.setMessage("Informe um n�mero de telefone v�lida.")
					.setNeutralButton("OK", null).show();
		} else if (edtDadosTelAdicional.getText().toString()
				.equals(VariaveisGlobais.contatoCelular)) {
			new AlertDialog.Builder(DadosPessoais.this)
					.setTitle("Aten��o")
					.setMessage(
							"Este telefone j� foi informado anteriormente.")
					.setNeutralButton("OK", null).show();
		} else if (edtDadosRenda.getText().toString().equals("")) {
			new AlertDialog.Builder(DadosPessoais.this).setTitle("Aten��o")
					.setMessage("Informe a renda.")
					.setNeutralButton("OK", null).show();
		} else {
			VariaveisGlobais.contatoNome = edtDadosNome.getText().toString();
			VariaveisGlobais.contatoDataNas = edtDadosDatNasc.getText()
					.toString();
			VariaveisGlobais.contatoTelefone = edtDadosTelAdicional.getText()
					.toString();
			VariaveisGlobais.contatoRendaMensal = edtDadosRenda.getText()
					.toString().substring(0, 2);
			Intent intent = new Intent(this, DadosPessoaisExtras.class);
			startActivity(intent);
			this.finish();
		}
	}

	public class ConsultaCpf extends AsyncTask<String, Void, Void> {
		public ProgressDialog pDialog;
		public JSONObject jsonEnvio = new JSONObject();
		public JSONObject jsonResposta1;
		public int jsonSucesso = 1;
		public int jsonAcao;
		public int jsonRetorno = 1;
		public String jsonDataNas;
		public String jsonNome;
		public String jsonMensagemErro;
		public boolean semConexao = true;
		List<String> jsonVencimento = new ArrayList<String>();

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			pDialog = new ProgressDialog(DadosPessoais.this);
			pDialog.setMessage("Aguarde um instante...");
			pDialog.setIndeterminate(false);
			pDialog.setCancelable(false);
			pDialog.show();
		}

		@Override
		protected Void doInBackground(String... params) {
			String cpfPessoa = params[0];
			try {
				semConexao = false;
				jsonEnvio.put("PARAM_NUMERO_CELULAR",
						VariaveisGlobais.contatoCelular);
				jsonEnvio.put("PARAM_NUMERO_CPF", cpfPessoa);
				jsonEnvio.put("PARAM_LOJISTA", 0);
				jsonResposta1 = getSetJson
						.enviarSolicitacao(openURL, jsonEnvio);
				jsonRetorno = jsonResposta1.getInt("RETORNO");// Pega o retorno;
				Log.i("Resposta", jsonResposta1.toString());
				if (jsonRetorno == 0) {
					JSONObject jsonResposta2 = jsonResposta1
							.getJSONObject("DADOS");
					jsonAcao = jsonResposta2.getInt("ACAO");
					if (jsonAcao == 0 || jsonAcao == 2) {
						jsonDataNas = jsonResposta2
								.getString("DATA_NASCIMENTO");
						jsonNome = jsonResposta2.getString("NOME");
						JSONObject jsonResposta22 = jsonResposta1
								.getJSONObject("DADOS");
						JSONArray jsonResposta33 = jsonResposta22
								.getJSONArray("VENCIMENTOS");
						for (int cont = 0; cont != jsonResposta33.length(); cont++) {
							VariaveisGlobais.jsonVencimento.add(jsonResposta33
									.getString(cont));
						}
					} else {
						jsonMensagemErro = jsonResposta2.getString("MENSAGEM");
					}
				} else {
					jsonMensagemErro = jsonResposta1.getString("MENSAGEM");
				}
			} catch (JSONException e) {
				Log.i("JSONException", e.getMessage());
				jsonRetorno = 1;
			} catch (ClientProtocolException e) {
				Log.i("ClientProtocolException", e.getMessage());
				jsonRetorno = 1;
			} catch (IOException e) {
				Log.i("IOException", e.getMessage());
				jsonRetorno = 1;
				semConexao = true;
			}
			return null;
		}

		@Override
		protected void onPostExecute(Void Void) {
			pDialog.dismiss();
			// Button btnCpfOk = (Button) findViewById(R.id.btnCpfOk);
			if (jsonRetorno == 0) {
				if (jsonAcao == 0 || jsonAcao == 2) {
					FrgDadosPessoais frgDadosPessoais = new FrgDadosPessoais();
					FragmentManager fManager = getSupportFragmentManager();
					FragmentTransaction fTransaction = fManager
							.beginTransaction();
					fTransaction.replace(R.id.frlDadosPessoais,
							frgDadosPessoais);
					fTransaction
							.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
					fTransaction.commit();
					// btnCpfOk.setVisibility(btnCpfOk.INVISIBLE);
					EditText edtDadosCpf = (EditText) findViewById(R.id.edtDadosCpf);
					VariaveisGlobais.contatoCpf = edtDadosCpf.getText()
							.toString();
				} else {
					new AlertDialog.Builder(DadosPessoais.this)
							.setTitle("Aten��o")
							.setMessage(jsonMensagemErro.toString())
							.setNeutralButton("OK", null).show();
				}
			} else {
				// btnCpfOk.setVisibility(btnCpfOk.VISIBLE);
				if (semConexao == false) {
					new AlertDialog.Builder(DadosPessoais.this)
							.setTitle("Aten��o")
							.setMessage(jsonMensagemErro)
							.setNeutralButton("OK", null).show();
				} else {
					new AlertDialog.Builder(DadosPessoais.this)
							.setTitle("Aten��o")
							.setMessage("Sem conex�o de rede.")
							.setNeutralButton("OK", null).show();
				}
			}
		}
	}
}
