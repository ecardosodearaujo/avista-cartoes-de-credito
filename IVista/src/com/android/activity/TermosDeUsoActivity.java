package com.android.activity;

import com.android.functions.VariaveisGlobais;
import com.android.projetoavista1.R;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.TextView;

public class TermosDeUsoActivity extends Activity{
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.termos_uso);
		
		TextView txtTermosUso = (TextView)findViewById(R.id.txtTermosUso);
		txtTermosUso.setText(VariaveisGlobais.aprovacaoTermos);
	}
	
	public void concordarTermos(View v){
		this.finish();
	}

}
