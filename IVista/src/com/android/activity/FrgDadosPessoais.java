package com.android.activity;

import com.android.functions.Mascaras;
import com.android.functions.VariaveisGlobais;
import com.android.projetoavista1.R;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.InputFilter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

public class FrgDadosPessoais extends Fragment {
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View view = inflater.inflate(
				com.android.projetoavista1.R.layout.fragment_dados_pessoais,
				container, false);

		EditText dataNasc = (EditText) view.findViewById(R.id.edtDadosDatNasc1);
		dataNasc.addTextChangedListener(Mascaras.insert("##/##/####", dataNasc));
		EditText telefonead = (EditText) view
				.findViewById(R.id.edtDadosTelAdicional);
		telefonead.addTextChangedListener(Mascaras.insert("(##) ####-####",
				telefonead));

		com.android.functions.EditTextMoeda renda = (com.android.functions.EditTextMoeda) view
				.findViewById(R.id.edtDadosRenda);
		// renda.addTextChangedListener(Mascaras.insert("#.###,##",renda));

		EditText edtDadosNome = (EditText) view.findViewById(R.id.edtDadosNome);
		edtDadosNome
				.setFilters(new InputFilter[] { new InputFilter.AllCaps() });

		if (VariaveisGlobais.voltarDadosPessoais == true) {
			edtDadosNome.setText(VariaveisGlobais.contatoNome);
			dataNasc.setText(VariaveisGlobais.contatoDataNas);
			telefonead.setText(VariaveisGlobais.contatoTelefone);
			renda.setText(VariaveisGlobais.contatoRendaMensal);
			VariaveisGlobais.voltarDadosPessoais = false;
		}
		return view;
	}
}