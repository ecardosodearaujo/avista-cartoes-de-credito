package com.android.activity;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.client.ClientProtocolException;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.android.functions.GetSetJSON;
import com.android.functions.VariaveisGlobais;
import com.android.projetoavista1.R;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.DialogInterface.OnClickListener;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;

public class DadosPessoaisExtras extends Activity {
	public String diaVencimento = null;
	public ProgressDialog pDialog;
	public int idPassada=-1;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.activity_dados_pessoais_extras);

		Spinner spnSexo = (Spinner) findViewById(R.id.spnSexo);

		new ConsultaEscolaridade().execute();
		new ConsultaEstadoCivil().execute();
		String[] sexo = { "SELECIONE", "Masculino", "Feminino" };

		ArrayAdapter<String> adapterSexo = new ArrayAdapter<String>(
				DadosPessoaisExtras.this, android.R.layout.simple_spinner_item,
				sexo);
		adapterSexo
				.setDropDownViewResource(android.R.layout.simple_spinner_item);
		spnSexo.setAdapter(adapterSexo);

		// Setando as datas nos botões:
		Button btn01 = (Button) findViewById(R.id.btn01);
		Button btn02 = (Button) findViewById(R.id.btn02);
		Button btn03 = (Button) findViewById(R.id.btn03);
		Button btn04 = (Button) findViewById(R.id.btn04);
		Button btn05 = (Button) findViewById(R.id.btn05);
		Button btn06 = (Button) findViewById(R.id.btn06);
		Button btn07 = (Button) findViewById(R.id.btn07);
		Button btn08 = (Button) findViewById(R.id.btn08);
		btn01.setText(VariaveisGlobais.jsonVencimento.get(0));
		btn02.setText(VariaveisGlobais.jsonVencimento.get(1));
		btn03.setText(VariaveisGlobais.jsonVencimento.get(2));
		btn04.setText(VariaveisGlobais.jsonVencimento.get(3));
		btn05.setText(VariaveisGlobais.jsonVencimento.get(4));
		btn06.setText(VariaveisGlobais.jsonVencimento.get(5));
		btn07.setText(VariaveisGlobais.jsonVencimento.get(6));
		btn08.setText(VariaveisGlobais.jsonVencimento.get(7));
	}

	@Override
	public void onBackPressed() {
		dialogFechar();
	}

	public void onClickExtrasBtnCancelar(View v) {
		dialogFechar();
	}

	

	public void dialogFechar() {
		new AlertDialog.Builder(DadosPessoaisExtras.this)
				.setTitle("Deseja Cancelar?")
				.setMessage(
						"Ao cancelar o processo este n�mero de celular n�o poder� ser ultilizado em uma nova proposta.")
				.setNegativeButton("N�o", new OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						dialog.dismiss();
					}
				}).setPositiveButton("Sim", new OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						finish();
					}
				}).show();
	}

	public void onClickFecharWindow(View v) {
		VariaveisGlobais.voltarDadosPessoais=true;
		Intent intent = new Intent(DadosPessoaisExtras.this,DadosPessoais.class);
		startActivity(intent);
		this.finish();
	}
	
	public void trocaCor(int idAtual){
		Button btnAtual = (Button) findViewById(idAtual);
		btnAtual.setBackgroundColor(Color.parseColor("#009966"));
		if (idPassada!=-1){
			Button btnPassado = (Button) findViewById(idPassada);
			btnPassado.setBackgroundColor(Color.parseColor("#A9A9A9"));
		}
	}

	public void onClickDiaVenciamento(View v) {
		switch (v.getId()) {

		case R.id.btn01:
			Button btn01 = (Button) findViewById(R.id.btn01);
			diaVencimento = btn01.getText().toString();
			trocaCor(R.id.btn01);
			idPassada=R.id.btn01;
			break;
		case R.id.btn02:
			Button btn02 = (Button) findViewById(R.id.btn02);
			diaVencimento = btn02.getText().toString();
			trocaCor(R.id.btn02);
			idPassada=R.id.btn02;
			break;
		case R.id.btn03:
			Button btn03 = (Button) findViewById(R.id.btn03);
			diaVencimento = btn03.getText().toString();
			trocaCor(R.id.btn03);
			idPassada=R.id.btn03;
			break;
		case R.id.btn04:
			Button btn04 = (Button) findViewById(R.id.btn04);
			diaVencimento = btn04.getText().toString();
			trocaCor(R.id.btn04);
			idPassada=R.id.btn04;
			break;
		case R.id.btn05:
			Button btn05 = (Button) findViewById(R.id.btn05);
			diaVencimento = btn05.getText().toString();
			trocaCor(R.id.btn05);
			idPassada=R.id.btn05;
			break;
		case R.id.btn06:
			Button btn06 = (Button) findViewById(R.id.btn06);
			diaVencimento = btn06.getText().toString();
			trocaCor(R.id.btn06);
			idPassada=R.id.btn06;
			break;
		case R.id.btn07:
			Button btn07 = (Button) findViewById(R.id.btn07);
			diaVencimento = btn07.getText().toString();
			trocaCor(R.id.btn07);
			idPassada=R.id.btn07;
			break;
		case R.id.btn08:
			Button btn08 = (Button) findViewById(R.id.btn08);
			diaVencimento = btn08.getText().toString();
			trocaCor(R.id.btn08);
			idPassada=R.id.btn08;
			break;
		}
	}

	public void onClickBtnExtrasContinuar(View v) {
		Spinner spnEscolaridade = (Spinner) findViewById(R.id.spnEscolaridade);
		Spinner spnEstadoCivil = (Spinner) findViewById(R.id.spnEstadoCivil);
		Spinner spnSexo = (Spinner) findViewById(R.id.spnSexo);

		if (spnEscolaridade.getSelectedItem().toString().equals("SELECIONE")) {
			new AlertDialog.Builder(DadosPessoaisExtras.this)
					.setTitle("Aten��o")
					.setMessage("Informe a Escolaridade.")
					.setNeutralButton("OK", null).show();
		} else if (spnEstadoCivil.getSelectedItem().toString()
				.equals("SELECIONE")) {
			new AlertDialog.Builder(DadosPessoaisExtras.this)
					.setTitle("Aten��o")
					.setMessage("Informe o estado civil.")
					.setNeutralButton("OK", null).show();
		} else if (spnSexo.getSelectedItem().toString().equals("SELECIONE")) {
			new AlertDialog.Builder(DadosPessoaisExtras.this)
					.setTitle("Aten��o").setMessage("Informe o sexo.")
					.setNeutralButton("OK", null).show();
		} else if (diaVencimento == null) {
			new AlertDialog.Builder(DadosPessoaisExtras.this)
					.setTitle("Aten��o")
					.setMessage("Informe o dia de vencimento.")
					.setNeutralButton("OK", null).show();
		} else {
			VariaveisGlobais.contatoEscolaridade = (spnEscolaridade
					.getSelectedItemPosition() + 1) + "";
			VariaveisGlobais.contatoEstadoCivil = spnEstadoCivil
					.getSelectedItem().toString().substring(0, 1);
			VariaveisGlobais.contatoSexo = spnSexo.getSelectedItem().toString()
					.substring(0, 1);
			VariaveisGlobais.dadosVencimento = diaVencimento;
			Intent intent = new Intent(this, EnderecoActivity.class);
			startActivity(intent);
			this.finish();
		}
	}

	public class ConsultaEscolaridade extends AsyncTask<Void, Void, Void> {
		public String openURL = "http://177.125.181.172/BossRest/nao/cliente/consultar/escolaridade/";
		public GetSetJSON getSetJson = new GetSetJSON();
		public JSONObject jsonResposta1;
		public JSONObject jsonEnvio = new JSONObject();
		public boolean semConexao = true;
		public int jsonRetorno = 0;
		public String jsonMensagem;
		List<String> jsonEscolaridade = new ArrayList<String>();
		public ArrayAdapter<String> adapter;
		
		
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			pDialog = new ProgressDialog(DadosPessoaisExtras.this);
			pDialog.setMessage("Aguarde um instante...");
			pDialog.setIndeterminate(false);
			pDialog.setCancelable(false);
			pDialog.show();
		}

		protected Void doInBackground(Void... params) {
			try {
				
				jsonEnvio.put("", "");
				jsonResposta1 = getSetJson
						.enviarSolicitacao(openURL, jsonEnvio);
				jsonRetorno = jsonResposta1.getInt("RETORNO");
				if (jsonRetorno == 0) {
					semConexao = false;
					JSONArray jsonResposta2 = jsonResposta1
							.getJSONArray("DADOS");
					jsonEscolaridade.add("SELECIONE");
					for (int cont = 0; cont != jsonResposta2.length(); cont++) {
						JSONArray jsonResposta3 = jsonResposta2
								.getJSONArray(cont);
						for (int cont1 = 0; cont1 != jsonResposta3.length(); cont1++) {
							if (cont1 == 1) {
								jsonEscolaridade.add(jsonResposta3
										.getString(cont1));
							}
						}
					}
				} else {
					jsonMensagem = jsonResposta1.getString("MENSAGEM");
				}

			} catch (JSONException e) {
				Log.i("JSONException", e.getMessage());
			} catch (ClientProtocolException e) {
				Log.i("ClientProtocolExceptionn", e.getMessage());
			} catch (IOException e) {
				semConexao = true;
				Log.i("IOException", e.getMessage());
			}
			return null;
		}

		@Override
		protected void onPostExecute(Void Void) {
			//pDialog.dismiss();
			if (jsonRetorno == 0) {
				Spinner spnEscolaridade = (Spinner) findViewById(R.id.spnEscolaridade);
				adapter = new ArrayAdapter<String>(DadosPessoaisExtras.this,
						android.R.layout.simple_spinner_item, jsonEscolaridade);
				adapter.setDropDownViewResource(android.R.layout.simple_spinner_item);
				spnEscolaridade.setAdapter(adapter);
			} else {
				if (semConexao == false) {
					new AlertDialog.Builder(DadosPessoaisExtras.this)
							.setTitle("Atenção")
							.setMessage(jsonMensagem.toString())
							.setNeutralButton("ok", null).show();
				} else {
					new AlertDialog.Builder(DadosPessoaisExtras.this)
							.setTitle("Atenção")
							.setMessage("Sem conexão de rede.")
							.setNeutralButton("ok", null).show();
				}
			}
		}
	}

	public class ConsultaEstadoCivil extends AsyncTask<Void, Void, Void> {
		public String openURL = "http://177.125.181.172/BossRest/nao/cliente/consultar/estado/civil/";
		public GetSetJSON getSetJson = new GetSetJSON();
		public JSONObject jsonResposta1;
		public JSONObject jsonEnvio = new JSONObject();
		public boolean semConexao = true;
		public int jsonRetorno = 0;
		public String jsonMensagem;
		List<String> jsonEstadoCivil = new ArrayList<String>();
		public ArrayAdapter<String> adapter;
		
		protected Void doInBackground(Void... params) {
			try {
				semConexao = false;
				jsonEnvio.put("", "");
				jsonResposta1 = getSetJson
						.enviarSolicitacao(openURL, jsonEnvio);
				jsonRetorno = jsonResposta1.getInt("RETORNO");
				if (jsonRetorno == 0) {
					JSONArray jsonResposta2 = jsonResposta1
							.getJSONArray("DADOS");
					jsonEstadoCivil.add("SELECIONE");
					for (int cont = 0; cont != jsonResposta2.length(); cont++) {
						JSONArray jsonResposta3 = jsonResposta2
								.getJSONArray(cont);
						for (int cont1 = 0; cont1 != jsonResposta3.length(); cont1++) {
							if (cont1 == 1) {
								jsonEstadoCivil.add(jsonResposta3
										.getString(cont1));
							}
						}
					}
				} else {
					jsonMensagem = jsonResposta1.getString("MENSAGEM");
				}

			} catch (JSONException e) {
				Log.i("JSONException", e.getMessage());
			} catch (ClientProtocolException e) {
				Log.i("ClientProtocolExceptionn", e.getMessage());
			} catch (IOException e) {
				semConexao = true;
				Log.i("IOException", e.getMessage());
			}
			return null;
		}

		@Override
		protected void onPostExecute(Void Void) {
			pDialog.dismiss();
			if (jsonRetorno == 0) {
				Spinner spnEstadoCivil = (Spinner) findViewById(R.id.spnEstadoCivil);
				adapter = new ArrayAdapter<String>(DadosPessoaisExtras.this,
						android.R.layout.simple_spinner_item, jsonEstadoCivil);
				adapter.setDropDownViewResource(android.R.layout.simple_spinner_item);
				spnEstadoCivil.setAdapter(adapter);
			} else {
				if (semConexao == false) {
					new AlertDialog.Builder(DadosPessoaisExtras.this)
							.setTitle("Aten��o")
							.setMessage(jsonMensagem.toString())
							.setNeutralButton("ok", null).show();
				} else {
					new AlertDialog.Builder(DadosPessoaisExtras.this)
							.setTitle("Aten��o")
							.setMessage("Sem conex�o de rede.")
							.setNeutralButton("ok", null).show();
				}
			}
		}
	}
}
