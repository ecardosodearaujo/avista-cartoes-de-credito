package com.android.activity;

import java.io.IOException;

import org.apache.http.client.ClientProtocolException;
import org.json.JSONException;
import org.json.JSONObject;

import com.android.functions.GetSetJSON;
import com.android.functions.Mascaras;
import com.android.functions.VariaveisGlobais;
import com.android.projetoavista1.R;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.DialogInterface.OnClickListener;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.EditText;
import android.widget.Spinner;

public class EnderecoActivity extends FragmentActivity {

	public GetSetJSON getSetJson = new GetSetJSON();
	boolean dadosEnviados = false;
	public String cepInformar;
	private static final String openURLCep = "http://177.125.181.172/BossRest/nao/cliente/consultar/cep/";
	private static final String openURLAprovar = "http://177.125.181.172/BossRest/nao/cliente/aprovar/";
	public String jsonBairro;
	public String jsonLogradouro;
	public String jsonCidade;
	public String jsonUF;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.activity_endereco);

		EditText cep = (EditText) findViewById(R.id.edtCep);
		cep.addTextChangedListener(Mascaras.insert("##.###-###", cep));
		if (VariaveisGlobais.enderecoBuscaCep == false) {
			if (VariaveisGlobais.enderecoMostraMensagemEndereco == true) {
				new AlertDialog.Builder(EnderecoActivity.this)
						.setTitle("Importante")
						.setMessage(
								"O Endere�o � de extrema import�ncia para a requisi��o do seu cart�o. Preencha-o corretamente.")
						.setNeutralButton("OK", null).show();
			}
		} else {
			cep.setText(VariaveisGlobais.enderecoCep);
			FrgEndereco Frgend = new FrgEndereco();
			FragmentManager fManager = getSupportFragmentManager();
			FragmentTransaction fTransaction = fManager.beginTransaction();
			fTransaction.replace(R.id.frlEndereco, Frgend);
			fTransaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
			fTransaction.commit();
		}
	}

	public void onClickEnderecoBtnOk(View v) {
		EditText edtCep = (EditText) findViewById(R.id.edtCep);
		if (!edtCep.getText().toString().equals("")) {
			new ConsultaCep().execute(edtCep.getText().toString());
		} else {
			new AlertDialog.Builder(EnderecoActivity.this)
					.setTitle("Aten��o").setMessage("Informe o CEP.")
					.setNeutralButton("OK", null).show();
		}
	}

	@Override
	public void onBackPressed() {
		dialogFechar();
	}

	public void onClickEnderecoBtnCancelar(View v) {
		dialogFechar();
	}

	public void dialogFechar() {
		new AlertDialog.Builder(EnderecoActivity.this)
				.setTitle("Deseja Cancelar?")
				.setMessage(
						"Ao cancelar o processo este n�mero de celular n�o poder� ser ultilizado em uma nova proposta.")
				.setNegativeButton("N�o", new OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						dialog.dismiss();
					}
				}).setPositiveButton("Sim", new OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						finish();
					}
				}).show();
	}

	public void onClickBtnContinuar(View v) {
		EditText edtCep = (EditText) findViewById(R.id.edtCep);
		Spinner spnTipo = (Spinner) findViewById(R.id.spnTipo);
		EditText edtLogradouro = (EditText) findViewById(R.id.edtLogradouro);

		EditText edtBairro = (EditText) findViewById(R.id.edtBairro);
		EditText edtCidade = (EditText) findViewById(R.id.edtCidade);
		Spinner spnEstado = (Spinner) findViewById(R.id.spnEstado);
		EditText edtNumero = (EditText) findViewById(R.id.edtNumero);
		EditText edtComplemento = (EditText) findViewById(R.id.edtComplemento);

		if (edtCep.getText().toString().equals("")) {
			new AlertDialog.Builder(EnderecoActivity.this)
					.setTitle("Aten��o").setMessage("Informe o CEP.")
					.setNeutralButton("OK", null).show();
		} else if (spnTipo.getSelectedItem().toString().equals("SELECIONE")) {
			new AlertDialog.Builder(EnderecoActivity.this)
					.setTitle("Aten��o")
					.setMessage("Informe o tipo de logradouro.")
					.setNeutralButton("OK", null).show();
		} else if (edtLogradouro.getText().toString().equals("")) {
			new AlertDialog.Builder(EnderecoActivity.this)
					.setTitle("Aten��o")
					.setMessage("Informe o logradouro.")
					.setNeutralButton("OK", null).show();
		} else if (edtNumero.getText().toString().equals("")) {
			new AlertDialog.Builder(EnderecoActivity.this)
					.setTitle("Aten��oo").setMessage("Informe o n�mero.")
					.setNeutralButton("OK", null).show();
		} else if (edtComplemento.getText().toString().equals("")) {
			new AlertDialog.Builder(EnderecoActivity.this)
					.setTitle("Aten��o")
					.setMessage("Informe o complemento.")
					.setNeutralButton("OK", null).show();
		} else if (edtBairro.getText().toString().equals("")) {
			new AlertDialog.Builder(EnderecoActivity.this)
					.setTitle("Aten��o").setMessage("Informe o bairro.")
					.setNeutralButton("OK", null).show();
		} else if (edtCidade.getText().toString().equals("")) {
			new AlertDialog.Builder(EnderecoActivity.this)
					.setTitle("Aten��o").setMessage("Informe a cidade.")
					.setNeutralButton("OK", null).show();
		} else if (spnEstado.getSelectedItem().toString().equals("SELECIONE")) {
			new AlertDialog.Builder(EnderecoActivity.this)
					.setTitle("Aten��o").setMessage("Informe o estado.")
					.setNeutralButton("OK", null).show();
		} else {
			// Passando dados da tela para as variaveis globais:
			VariaveisGlobais.enderecoUf = spnEstado.getSelectedItem()
					.toString();
			VariaveisGlobais.enderecoCidade = edtCidade.getText().toString();
			VariaveisGlobais.enderecoBairro = edtBairro.getText().toString();
			VariaveisGlobais.enderecoLogradouro = edtLogradouro.getText()
					.toString();
			VariaveisGlobais.enderecoNumero = edtNumero.getText().toString();
			VariaveisGlobais.enderecoComplemento = edtComplemento.getText()
					.toString();
			new ValidarCliente().execute();
		}
	}

	public void naoSeiMeuCep(View v) {
		switch (v.getId()) {
		case R.id.BtnNaoSeiCep:
			Intent intent = new Intent(getBaseContext(), BuscaCepActivity.class);
			startActivity(intent);
			EnderecoActivity.this.finish();
			break;
		}
	}

	public class ValidarCliente extends AsyncTask<Void, Void, Void> {
		public ProgressDialog pDialog;
		public JSONObject jsonEnvio = new JSONObject();
		public JSONObject jsonResposta1;
		public JSONObject jsonResposta2;
		public int jsonSucesso = 1;
		public int jsonRetorno = 1;
		public boolean semConexao = true;
		public String jsonTermo;
		public String jsonTarifa;
		public String jsonMensagem;
		public String jsonCodigo;
		public int jsonTipoAprovado;

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			pDialog = new ProgressDialog(EnderecoActivity.this);
			pDialog.setMessage("Aguarde um instante...");
			pDialog.setIndeterminate(false);
			pDialog.setCancelable(false);
			pDialog.show();
		}

		@Override
		protected Void doInBackground(Void... params) {
			try {
				semConexao = false;
				// Pegando os dados da tela:
				Spinner spnUF = (Spinner) findViewById(R.id.spnEstado);
				EditText edtCidade = (EditText) findViewById(R.id.edtCidade);
				EditText edtBairro = (EditText) findViewById(R.id.edtBairro);
				EditText edtLogradouro = (EditText) findViewById(R.id.edtLogradouro);
				EditText edtNumero = (EditText) findViewById(R.id.edtNumero);
				EditText edtComplemento = (EditText) findViewById(R.id.edtComplemento);

				// Passando dados da tela para as variaveis globais:
				VariaveisGlobais.enderecoUf = spnUF.getSelectedItem()
						.toString();
				VariaveisGlobais.enderecoCidade = edtCidade.getText()
						.toString();
				VariaveisGlobais.enderecoBairro = edtBairro.getText()
						.toString();
				VariaveisGlobais.enderecoLogradouro = edtLogradouro.getText()
						.toString();
				VariaveisGlobais.enderecoNumero = edtNumero.getText()
						.toString();
				VariaveisGlobais.enderecoComplemento = edtComplemento.getText()
						.toString();

				// Montando o Json:
				jsonEnvio.put("PARAM_CODIGO_UNIDADE_CREDENCIADA",
						VariaveisGlobais.unidadeCredenciada);
				jsonEnvio.put("PARAM_CODIGO_PROMOTER",
						VariaveisGlobais.codigoPromoter);
				jsonEnvio.put("PARAM_NUMERO_CELULAR",
						VariaveisGlobais.contatoCelular);
				jsonEnvio.put("PARAM_NUMERO_TELEFONE",
						VariaveisGlobais.contatoTelefone);
				jsonEnvio.put("PARAM_NUMERO_CPF", VariaveisGlobais.contatoCpf);
				jsonEnvio.put("PARAM_NOME", VariaveisGlobais.contatoNome);
				jsonEnvio.put("PARAM_DATA_NASCIMENTO",
						VariaveisGlobais.contatoDataNas);
				jsonEnvio.put("PARAM_ESCOLARIDADE",
						VariaveisGlobais.contatoEscolaridade);
				jsonEnvio.put("PARAM_ESTADO_CIVIL",
						VariaveisGlobais.contatoEstadoCivil);
				jsonEnvio.put("PARAM_SEXO", VariaveisGlobais.contatoSexo);
				jsonEnvio.put("PARAM_DADOS_ALTERADOS",
						VariaveisGlobais.dadosAlterados);
				jsonEnvio.put("PARAM_RENDA_MENSAL",
						VariaveisGlobais.contatoRendaMensal);
				jsonEnvio.put("PARAM_VENCIMENTO",
						VariaveisGlobais.dadosVencimento);
				jsonEnvio.put("PARAM_NUMERO_CEP", VariaveisGlobais.enderecoCep);
				jsonEnvio.put("PARAM_UF", spnUF.getSelectedItem().toString());
				jsonEnvio.put("PARAM_CIDADE", edtCidade.getText().toString());
				jsonEnvio.put("PARAM_BAIRRO", edtBairro.getText().toString());
				jsonEnvio.put("PARAM_LOGRADOURO", edtLogradouro.getText()
						.toString());
				jsonEnvio.put("PARAM_NUMERO", edtNumero.getText().toString());
				jsonEnvio.put("PARAM_COMPLEMENTO", edtComplemento.getText()
						.toString());
				Log.i("JsonEnvio", jsonEnvio.toString());

				jsonResposta1 = getSetJson.enviarSolicitacao(openURLAprovar,
						jsonEnvio);
				Log.i("Log Final: ", jsonResposta1.toString());
				jsonRetorno = jsonResposta1.getInt("RETORNO");// Pega o retorno;
				if (jsonRetorno == 0) {
					jsonResposta2 = jsonResposta1.getJSONObject("DADOS");
					jsonTarifa = jsonResposta2.getString("TARIFA");
					Log.i("Tarifa", jsonTarifa);
					jsonTermo = jsonResposta2.getString("TERMO");
					Log.i("Termos", jsonTermo);
					jsonTipoAprovado = jsonResposta2.getInt("APROVADO");
					Log.i("Aprovado", jsonTipoAprovado + "");
					if (jsonTipoAprovado == 0) {
						jsonCodigo = jsonResposta2.getString("CODIGO");
						Log.i("Codigo", jsonCodigo + "");
					}
					jsonMensagem = jsonResposta2.getString("MENSAGEM");
					Log.i("Mesagem", jsonMensagem);
				} else {
					jsonMensagem = jsonResposta1.getString("MENSAGEM");
				}
			} catch (JSONException e) {
				Log.i("JSONException", e.getMessage());
				jsonRetorno = 1;
			} catch (ClientProtocolException e) {
				Log.i("ClientProtocolException", e.getMessage());
				jsonRetorno = 1;
				semConexao = true;
			} catch (IOException e) {
				Log.i("IOException", e.getMessage());
				jsonRetorno = 1;
			}
			return null;
		}

		@Override
		protected void onPostExecute(Void Void) {
			pDialog.dismiss();
			if (jsonRetorno == 0) {
				dadosEnviados = true;
				// Guardo os dados para as proximas telas:
				VariaveisGlobais.aprovacaoTipoCartao = jsonTipoAprovado;
				VariaveisGlobais.aprovacaoTarifa = jsonTarifa;
				VariaveisGlobais.aprovacaoTermos = jsonTermo;
				// Alerta da proposta aprovada:
				new AlertDialog.Builder(EnderecoActivity.this)
						.setTitle("Atenção").setMessage(jsonMensagem)
						.setPositiveButton("OK", new OnClickListener() {
							@Override
							public void onClick(DialogInterface dialog,
									int which) {
								if (jsonTipoAprovado == 0) {// Aprovado:
															// Crédito;
									Intent intent = new Intent(
											EnderecoActivity.this,
											MinhasFotosActivity.class);
									startActivity(intent);
								} else {// Aprovado: Pré pago;
									VariaveisGlobais.aprovacaoCodigo = jsonCodigo;
									// Variaveis globais
									Intent intent = new Intent(
											EnderecoActivity.this,
											AssinaturaActivity.class);
									startActivity(intent);
								}
							}
						}).show();
			} else {
				if (semConexao == false) {
					new AlertDialog.Builder(EnderecoActivity.this)
							.setTitle("Aten��o").setMessage(jsonMensagem)
							.setPositiveButton("OK", new OnClickListener() {
								@Override
								public void onClick(DialogInterface dialog,
										int which) {
									Intent intent = new Intent(
											EnderecoActivity.this,
											AreaClienteActivity.class);
									startActivity(intent);
									EnderecoActivity.this.finish();
								}
							}).show();
				} else {
					new AlertDialog.Builder(EnderecoActivity.this)
							.setTitle("Aten��o")
							.setMessage("Sem conex�o de rede.")
							.setNeutralButton("OK", null).show();
				}
			}
		}
	}

	public class ConsultaCep extends AsyncTask<String, Void, Void> {
		public ProgressDialog pDialog;
		public JSONObject jsonEnvio = new JSONObject();
		public JSONObject jsonResposta1;
		public int jsonSucesso = 1;
		public int jsonAcao;
		public int jsonRetorno = 1;
		public String jsonMensagemErro;
		public boolean semConexao = true;

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			pDialog = new ProgressDialog(EnderecoActivity.this);
			pDialog.setMessage("Aguarde um instante...");
			pDialog.setIndeterminate(false);
			pDialog.setCancelable(false);
			pDialog.show();
		}

		@Override
		protected Void doInBackground(String... params) {
			String cepInformar = params[0];
			try {
				semConexao = false;
				jsonEnvio.put("PARAM_NUMERO_CEP", cepInformar);
				jsonResposta1 = getSetJson.enviarSolicitacao(openURLCep,
						jsonEnvio);
				jsonRetorno = jsonResposta1.getInt("RETORNO");
				if (jsonRetorno == 0) {
					JSONObject jsonResposta2 = jsonResposta1
							.getJSONObject("DADOS");

					jsonBairro = jsonResposta2.getString("BAIRRO");
					jsonLogradouro = jsonResposta2.getString("LOGRADOURO");
					jsonCidade = jsonResposta2.getString("CIDADE");
					jsonUF = jsonResposta2.getString("UF");
					VariaveisGlobais.enderecoUf = jsonUF;
					VariaveisGlobais.enderecoCidade = jsonCidade;
					VariaveisGlobais.enderecoBairro = jsonBairro;
					VariaveisGlobais.enderecoLogradouro = jsonLogradouro;

				} else {
					jsonMensagemErro = jsonResposta1.getString("MENSAGEM");
				}
			} catch (JSONException e) {
				Log.i("JSONException", e.getMessage());
				jsonRetorno = 1;
			} catch (ClientProtocolException e) {
				Log.i("ClientProtocolException", e.getMessage());
				jsonRetorno = 1;
				semConexao = true;
			} catch (IOException e) {
				Log.i("IOException", e.getMessage());
				jsonRetorno = 1;
			}
			return null;
		}

		@Override
		protected void onPostExecute(Void Void) {
			// Button btnOk = (Button) findViewById(R.id.btnOk);
			pDialog.dismiss();
			if (jsonRetorno == 0) {
				dadosEnviados = true;
				FrgEndereco Frgend = new FrgEndereco();
				FragmentManager fManager = getSupportFragmentManager();
				FragmentTransaction fTransaction = fManager.beginTransaction();
				fTransaction.replace(R.id.frlEndereco, Frgend);
				fTransaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
				fTransaction.commit();
				// btnOk.setVisibility(btnOk.INVISIBLE);
				EditText edtCep = (EditText) findViewById(R.id.edtCep);
				VariaveisGlobais.enderecoCep = edtCep.getText().toString();
			} else {
				// btnOk.setVisibility(btnOk.VISIBLE);
				if (semConexao == false) {
					new AlertDialog.Builder(EnderecoActivity.this)
							.setTitle("Aten��o")
							.setMessage(jsonMensagemErro)
							.setNeutralButton("OK", null).show();
				} else {
					new AlertDialog.Builder(EnderecoActivity.this)
							.setTitle("Aten��o")
							.setMessage("Sem conex�o de rede.")
							.setNeutralButton("OK", null).show();
				}
			}
		}
	}
}
