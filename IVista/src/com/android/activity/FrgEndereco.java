package com.android.activity;

import com.android.functions.VariaveisGlobais;
import com.android.projetoavista1.R;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.InputFilter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;

public class FrgEndereco extends Fragment {

	EditText bairro;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View view = inflater.inflate(
				com.android.projetoavista1.R.layout.fragment_endereco,
				container, false);

		Spinner spnTipo = (Spinner) view.findViewById(R.id.spnTipo);
		Spinner spnEstado = (Spinner) view.findViewById(R.id.spnEstado);
		EditText edtLogradouro = (EditText) view
				.findViewById(R.id.edtLogradouro);
		EditText edtComplemento = (EditText) view
				.findViewById(R.id.edtComplemento);
		EditText edtBairro = (EditText) view.findViewById(R.id.edtBairro);
		EditText edtCidade = (EditText) view.findViewById(R.id.edtCidade);

		edtCidade.setText(VariaveisGlobais.enderecoCidade.toString());
		edtBairro.setText(VariaveisGlobais.enderecoBairro.toString());
		edtLogradouro.setText(VariaveisGlobais.enderecoLogradouro.toString());

		VariaveisGlobais.enderecoBuscaCep = false;
		VariaveisGlobais.enderecoJaInstanciada = true;

		edtLogradouro
				.setFilters(new InputFilter[] { new InputFilter.AllCaps() });
		edtComplemento
				.setFilters(new InputFilter[] { new InputFilter.AllCaps() });
		edtBairro.setFilters(new InputFilter[] { new InputFilter.AllCaps() });
		edtCidade.setFilters(new InputFilter[] { new InputFilter.AllCaps() });

		String[] tipo = { "SELECIONE", "AEROPORTO", "ALAMEDA", "ÁREA",
				"AVENIDA", "CAMPO", "CHÁCARA", "COLÔNIA", "CONDOMINIO",
				"CONJUNTO", "DISTRITO", "ESPLANADA", "ESTAÇÃO", "ESTRADA",
				"FAVELA", "FAZENDA", "FEIRA", "JARDIM", "LADEIRA", "LAGO",
				"LÁGOA", "LARGO", "LOTEAMENTO", "MORRO", "NÚCLEO", "PARQUE",
				"PASSARELA", "PÁTIO", "PRAÇA", "QUADRA", "RECANTO",
				"RESIDENCIAL", "RODOVIA", "RUA", "SETOR", "SITIO", "TRECHO",
				"TREVO", "VALE", "VEREDA", "VIA", "VIADUTO", "VIELA", "VILA" };

		String[] estado = { "SELECIONE", "AC", "AL", "AP", "AM", "BA", "CE",
				"DF", "ES", "GO", "MA", "MT", "MS", "MG", "PA", "PB", "PR",
				"PE", "PI", "RJ", "RN", "RS", "RO", "RR", "SC", "SP", "SE",
				"TO" };

		ArrayAdapter<String> adapterTipo = new ArrayAdapter<String>(
				this.getActivity(), android.R.layout.simple_spinner_item, tipo);
		ArrayAdapter<String> adapterEstado = new ArrayAdapter<String>(
				this.getActivity(), android.R.layout.simple_spinner_item,
				estado);

		adapterTipo
				.setDropDownViewResource(android.R.layout.simple_spinner_item);
		adapterEstado
				.setDropDownViewResource(android.R.layout.simple_spinner_item);

		spnTipo.setAdapter(adapterTipo);
		spnEstado.setAdapter(adapterEstado);

		spnEstado.setSelection(setEstado(VariaveisGlobais.enderecoUf.trim()));

		return view;
	}

	public int setEstado(String UF) {
		if (UF.equals("AC")) {
			return 1;
		} else if (UF.equals("AL")) {
			return 2;
		} else if (UF.equals("AP")) {
			return 3;
		} else if (UF.equals("AM")) {
			return 4;
		} else if (UF.equals("BA")) {
			return 5;
		} else if (UF.equals("CE")) {
			return 6;
		} else if (UF.equals("DF")) {
			return 7;
		} else if (UF.equals("ES")) {
			return 8;
		} else if (UF.equals("GO")) {
			return 9;
		} else if (UF.equals("MA")) {
			return 10;
		} else if (UF.equals("MT")) {
			return 1;
		} else if (UF.equals("MS")) {
			return 12;
		} else if (UF.equals("MG")) {
			return 13;
		} else if (UF.equals("PA")) {
			return 14;
		} else if (UF.equals("PB")) {
			return 15;
		} else if (UF.equals("PR")) {
			return 16;
		} else if (UF.equals("PE")) {
			return 17;
		} else if (UF.equals("PI")) {
			return 18;
		} else if (UF.equals("RJ")) {
			return 19;
		} else if (UF.equals("RN")) {
			return 20;
		} else if (UF.equals("RS")) {
			return 21;
		} else if (UF.equals("RO")) {
			return 22;
		} else if (UF.equals("RR")) {
			return 23;
		} else if (UF.equals("SC")) {
			return 24;
		} else if (UF.equals("SP")) {
			return 25;
		} else if (UF.equals("SE")) {
			return 26;
		} else if (UF.equals("TO")) {
			return 27;
		}
		return 0;
	}
}
