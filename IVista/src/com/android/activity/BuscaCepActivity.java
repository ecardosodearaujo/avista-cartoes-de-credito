package com.android.activity;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import org.apache.http.client.ClientProtocolException;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.android.functions.GetSetJSON;
import com.android.functions.VariaveisGlobais;
import com.android.projetoavista1.R;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.InputFilter;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.Spinner;
import android.widget.AdapterView.OnItemClickListener;

public class BuscaCepActivity extends Activity {

	public ListView lista;
	private ArrayList<HashMap<String, String>> mEnderecoList;
	HashMap<String, String> lojaEndereco = new HashMap<String, String>();
	boolean jaEscolheu = false;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.activity_busca_cep);

		Spinner spnEstado = (Spinner) findViewById(R.id.spnEstadoValidaCep);
		EditText edtCidadeValidaCep = (EditText) findViewById(R.id.edtCidadeValidaCep);
		EditText edtLogradouroValidaCep = (EditText) findViewById(R.id.edtLogradouroValidaCep);
		ListView lstvCeps = (ListView) findViewById(R.id.lstvCeps);
		edtCidadeValidaCep
				.setFilters(new InputFilter[] { new InputFilter.AllCaps() });
		edtLogradouroValidaCep
				.setFilters(new InputFilter[] { new InputFilter.AllCaps() });

		String[] estado = { "SELECIONE", "AC", "AL", "AP", "AM", "BA", "CE",
				"DF", "ES", "GO", "MA", "MT", "MS", "MG", "PA", "PB", "PR",
				"PE", "PI", "RJ", "RN", "RS", "RO", "RR", "SC", "SP", "SE",
				"TO" };

		ArrayAdapter<String> adapterEstado = new ArrayAdapter<String>(
				BuscaCepActivity.this, android.R.layout.simple_spinner_item,
				estado);

		adapterEstado
				.setDropDownViewResource(android.R.layout.simple_spinner_item);

		spnEstado.setAdapter(adapterEstado);

		lstvCeps.setOnItemClickListener(new OnItemClickListener() {
			public void onItemClick(AdapterView<?> arg0, View arg1, int pst,
					long id) {
				lojaEndereco = mEnderecoList.get(pst);
				jaEscolheu = true;
			}
		});
	}

	public void onClickSelecionarOpcao(View v) {
		VariaveisGlobais.enderecoUf = lojaEndereco.get("UF");
		VariaveisGlobais.enderecoCidade = lojaEndereco.get("CIDADE");
		VariaveisGlobais.enderecoLogradouro = lojaEndereco.get("LOGRADOURO");
		VariaveisGlobais.enderecoBairro = lojaEndereco.get("BAIRRO");
		VariaveisGlobais.enderecoCep = lojaEndereco.get("CEP");
		VariaveisGlobais.enderecoBuscaCep = true;
		if (jaEscolheu == true) {
			Intent intent = new Intent(BuscaCepActivity.this,
					EnderecoActivity.class);
			startActivity(intent);
			BuscaCepActivity.this.finish();
		} else {
			new AlertDialog.Builder(BuscaCepActivity.this).setTitle("Aten��o")
					.setMessage("Selecione um CEP.")
					.setNeutralButton("OK", null).show();
		}
	}

	public void detalhaCep(View v) {
		Spinner spnEstado = (Spinner) findViewById(R.id.spnEstadoValidaCep);
		EditText edtCidadeValidaCep = (EditText) findViewById(R.id.edtCidadeValidaCep);
		EditText edtLogradouroValidaCep = (EditText) findViewById(R.id.edtLogradouroValidaCep);

		if (spnEstado.getSelectedItem().toString().trim().equals("SELECIONE")) {
			new AlertDialog.Builder(BuscaCepActivity.this)
					.setTitle("Aten��o").setMessage("Informe o estado.")
					.setNeutralButton("OK", null).show();
		} else if (edtCidadeValidaCep.getText().toString().trim().equals("")) {
			new AlertDialog.Builder(BuscaCepActivity.this)
					.setTitle("Aten��o")
					.setMessage("Informe o informe a localidade.")
					.setNeutralButton("OK", null).show();
		} else if (edtLogradouroValidaCep.getText().toString().trim()
				.equals("")) {
			new AlertDialog.Builder(BuscaCepActivity.this)
					.setTitle("Aten��o").setMessage("Informe o logradouro.")
					.setNeutralButton("OK", null).show();
		} else {
			new ConsultaCep().execute();
		}
	}

	public void onClickBtnCancelar(View v) {
		fecharWindows();
	}

	@Override
	public void onBackPressed() {
		fecharWindows();
	}

	public void fecharWindows() {
		if (VariaveisGlobais.enderecoJaInstanciada == true) {
			VariaveisGlobais.enderecoBuscaCep = true;
		} else {
			VariaveisGlobais.enderecoBuscaCep = false;
			VariaveisGlobais.enderecoMostraMensagemEndereco = false;
		}
		Intent intent = new Intent(BuscaCepActivity.this,
				EnderecoActivity.class);
		startActivity(intent);
		BuscaCepActivity.this.finish();
	}

	public class ConsultaCep extends AsyncTask<Void, Void, Void> {
		public GetSetJSON getSetJson = new GetSetJSON();
		public ProgressDialog pDialog;
		public String openURL = "http://177.125.181.172/BossRest/nao/cliente/consultar/cep/logradouro/";
		JSONObject jsonEnvio = new JSONObject();
		JSONObject jsonResposta1 = new JSONObject();
		boolean semConexao = true;
		public int jsonRetorno = 1;
		public String jsonMesagem;
		JSONArray jsonResposta2 = new JSONArray();

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			pDialog = new ProgressDialog(BuscaCepActivity.this);
			pDialog.setMessage("Aguarde um instante...");
			pDialog.setIndeterminate(false);
			pDialog.setCancelable(false);
			pDialog.show();
		}

		@Override
		protected Void doInBackground(Void... arg0) {
			mEnderecoList = new ArrayList<HashMap<String, String>>();
			GetSetJSON getSetJson = new GetSetJSON();
			try {
				semConexao = false;
				Spinner spnUFnc = (Spinner) findViewById(R.id.spnEstadoValidaCep);
				EditText edtCidadenc = (EditText) findViewById(R.id.edtCidadeValidaCep);
				EditText edtBairronc = (EditText) findViewById(R.id.edtLogradouroValidaCep);

				jsonEnvio.put("PARAM_UF", spnUFnc.getSelectedItem().toString());
				jsonEnvio.put("PARAM_CIDADE", edtCidadenc.getText().toString());
				jsonEnvio.put("PARAM_BAIRRO", "");
				jsonEnvio.put("PARAM_LOGRADOURO", edtBairronc.getText()
						.toString());

				Log.i("Json de Envio:", jsonEnvio.toString());

				JSONObject jsonResposta1 = getSetJson.enviarSolicitacao(
						openURL, jsonEnvio);
				jsonRetorno = jsonResposta1.getInt("RETORNO");
				if (jsonRetorno == 0) {
					jsonResposta2 = jsonResposta1.getJSONArray("DADOS");
					for (int cont = 0; cont != jsonResposta2.length(); cont++) {
						JSONArray jsonResposta3 = jsonResposta2
								.getJSONArray(cont);
						HashMap<String, String> map = new HashMap<String, String>();
						map.put("UF", jsonResposta3.getString(0));
						map.put("CIDADE", jsonResposta3.getString(1));
						map.put("LOGRADOURO", jsonResposta3.getString(2));
						map.put("BAIRRO", jsonResposta3.getString(3));
						map.put("CEP", jsonResposta3.getString(4));
						mEnderecoList.add(map);
					}
				} else {
					jsonMesagem = jsonResposta1.getString("MENSAGEM");
				}
			} catch (JSONException e) {
				Log.i("JSONException", e.getMessage());
				jsonRetorno = 1;
			} catch (ClientProtocolException e) {
				Log.i("ClientProtocolException", e.getMessage());
				jsonRetorno = 1;
			} catch (IOException e) {
				Log.i("IOException", e.getMessage());
				jsonRetorno = 1;
				semConexao = true;
			}
			return null;
		}

		@Override
		protected void onPostExecute(Void Void) {
			pDialog.dismiss();
			if (jsonRetorno == 0) {
				if (jsonResposta2.length() == 0) {
					new AlertDialog.Builder(BuscaCepActivity.this)
							.setTitle("Aten��o")
							.setMessage("Nenhum resultado encontrado.")
							.setNeutralButton("OK", null).show();
					ListView lstvCeps = (ListView) findViewById(R.id.lstvCeps);
					lstvCeps.setAdapter(null);
				} else {
					ListAdapter adapter = new SimpleAdapter(
							BuscaCepActivity.this, mEnderecoList,
							R.layout.item_lista_cep, new String[] { "CEP",
									"LOGRADOURO", "BAIRRO" }, new int[] {
									R.id.txtcep, R.id.txtlogradouro,
									R.id.txtbairro });
					ListView lstvCeps = (ListView) findViewById(R.id.lstvCeps);
					lstvCeps.setAdapter(adapter);
				}
			} else {
				if (semConexao == false) {
					new AlertDialog.Builder(BuscaCepActivity.this)
							.setTitle("Aten��o").setMessage(jsonMesagem)
							.setNeutralButton("OK", null).show();
				} else {
					new AlertDialog.Builder(BuscaCepActivity.this)
							.setTitle("Aten��o")
							.setMessage("Sem conex�o de rede.")
							.setNeutralButton("OK", null).show();

				}
			}
		}
	}
}
