package com.android.activity;

import com.android.functions.Mascaras;
import com.android.projetoavista1.R;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

public class FrgCodSeguranca extends Fragment {
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View view = inflater.inflate(
				com.android.projetoavista1.R.layout.fragment_codigo_seguranca,
				container, false);
		
		EditText codseguro = (EditText) view.findViewById(R.id.edtCodSeguranca);

		codseguro.addTextChangedListener(Mascaras.insert("#####",
				codseguro));

		return view;
	}
}