package com.android.activity;

import java.io.IOException;

import org.apache.http.client.ClientProtocolException;
import org.json.JSONException;
import org.json.JSONObject;

import com.android.functions.GetSetJSON;
import com.android.functions.MyCountDownTimer;
import com.android.functions.VariaveisGlobais;
import com.android.projetoavista1.R;
import com.pascalwelsch.holocircularprogressbar.HoloCircularProgressBar;

import android.animation.Animator;
import android.animation.ObjectAnimator;
import android.animation.ValueAnimator;
import android.animation.Animator.AnimatorListener;
import android.animation.ValueAnimator.AnimatorUpdateListener;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.DialogInterface.OnClickListener;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.Window;
import android.widget.Chronometer;
import android.widget.Toast;

public class TimerActivity extends Activity {
	private MyCountDownTimer timer;
	private Chronometer tv;
	private ObjectAnimator mProgressBarAnimator;

	// private static final int PROGRESS = 0x1;
	// private ProgressBar mProgress;
	@SuppressWarnings("unused")
	private int mProgressStatus;
	private boolean acabouTempo = false;
	private HoloCircularProgressBar mHoloCircularProgressBar;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.activity_timer);

		mHoloCircularProgressBar = (HoloCircularProgressBar) findViewById(R.id.holoCircularProgressBar);
		animate(mHoloCircularProgressBar, null, 1f,
				60000 * VariaveisGlobais.tempoAprovacao);
		mHoloCircularProgressBar.setMarkerProgress(1f);
		tv = (Chronometer) findViewById(R.id.chronometer);
		timer = new MyCountDownTimer(this, tv,
				VariaveisGlobais.tempoAprovacao * 60 * 1000, 1000);
		timer.start();
		// mProgress = (ProgressBar) findViewById(R.id.progress);
		new TimerProgress().execute();
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
		if (timer != null) {
			timer.cancel();
		}
	}

	private void animate(final HoloCircularProgressBar progressBar,
			final AnimatorListener listener, final float progress,
			final int duration) {

		mProgressBarAnimator = ObjectAnimator.ofFloat(progressBar, "progress",
				progress);
		mProgressBarAnimator.setDuration(duration);

		mProgressBarAnimator.addListener(new AnimatorListener() {

			@Override
			public void onAnimationCancel(final Animator animation) {
			}

			@Override
			public void onAnimationEnd(final Animator animation) {
				progressBar.setProgress(progress);
			}

			@Override
			public void onAnimationRepeat(final Animator animation) {
			}

			@Override
			public void onAnimationStart(final Animator animation) {

			}
		});
		if (listener != null) {
			mProgressBarAnimator.addListener(listener);
		}
		mProgressBarAnimator.reverse();
		mProgressBarAnimator.addUpdateListener(new AnimatorUpdateListener() {

			@Override
			public void onAnimationUpdate(final ValueAnimator animation) {
				progressBar.setProgress((Float) animation.getAnimatedValue());
			}
		});
		progressBar.setMarkerProgress(progress);
		mProgressBarAnimator.start();
	}

	@Override
	public void onBackPressed() {
		new AlertDialog.Builder(TimerActivity.this)
				.setTitle("Deseja Cancelar?")
				.setMessage(
						"Ao cancelar o processo este n�mero de celular n�o poder� ser ultilizado em uma nova proposta.")
				.setNegativeButton("N�o", new OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						dialog.dismiss();
					}
				}).setPositiveButton("Sim", new OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						finish();
					}
				}).show();
	}

	public class TimerProgress extends AsyncTask<Void, Boolean, Boolean> {
		protected Boolean doInBackground(Void... params) {
			while (acabouTempo != true) {
				acabouTempo = VariaveisGlobais.acabou;
				if (acabouTempo == true) {
					VariaveisGlobais.acabou = false;
				}
				mProgressStatus++;
			}
			return acabouTempo;
		}

		@Override
		protected void onPostExecute(Boolean progresso) {
			if (acabouTempo == true) {
				new VerificaAprovacao().execute();
			}
		}
	}

	public class VerificaAprovacao extends AsyncTask<Void, Void, Void> {
		public String openURL = "http://177.125.181.172/BossRest/nao/cliente/verificar/aprovacao/";
		public GetSetJSON getSetJson = new GetSetJSON();
		public JSONObject jsonResposta1;
		public JSONObject jsonResposta2;
		public JSONObject jsonEnvio = new JSONObject();
		public boolean semConexao = true;
		public int jsonRetorno = -1;
		public String jsonMensagem;
		public ProgressDialog pDialog;
		public int jsonProcessado = -1;
		public int jsonAprovado = -1;

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			pDialog = new ProgressDialog(TimerActivity.this);
			pDialog.setMessage("Aguarde um instante...");
			pDialog.setIndeterminate(false);
			pDialog.setCancelable(false);
			pDialog.show();
		}

		@Override
		protected Void doInBackground(Void... params) {
			try {
				semConexao = false;
				jsonEnvio.put("PARAM_CODIGO_PROPOSTA",
						VariaveisGlobais.aprovacaoCodigo + "");
				jsonResposta1 = getSetJson
						.enviarSolicitacao(openURL, jsonEnvio);
				Log.i("JSON ENVIO", jsonResposta1.toString());
				jsonRetorno = jsonResposta1.getInt("RETORNO");
				if (jsonRetorno == 0) {
					jsonResposta2 = jsonResposta1.getJSONObject("DADOS");
					jsonProcessado = jsonResposta2.getInt("PROCESSADO");
					jsonAprovado = jsonResposta2.getInt("APROVADO");
				} else {
					jsonMensagem = jsonResposta1.getString("MENSAGEM");
				}
			} catch (JSONException e) {
				Log.i("JSONException", e.getMessage());
			} catch (ClientProtocolException e) {
				Log.i("ClientProtocolException", e.getMessage());
			} catch (IOException e) {
				semConexao = true;
				Log.i("IOException", e.getMessage());
			}
			return null;
		}

		@Override
		protected void onPostExecute(Void Void) {
			pDialog.dismiss();
			if (jsonRetorno == 0) {
				if (jsonProcessado == 0) {
					Toast.makeText(
							TimerActivity.this,
							"Proposta ainda n�o processada. Por favor aguarde mais 2 minutos...",
							Toast.LENGTH_LONG).show();
					mHoloCircularProgressBar = (HoloCircularProgressBar) findViewById(R.id.holoCircularProgressBar);
					animate(mHoloCircularProgressBar, null, 1f, 60000 * 2);
					mHoloCircularProgressBar.setMarkerProgress(1f);
					tv = (Chronometer) findViewById(R.id.chronometer);
					timer = new MyCountDownTimer(TimerActivity.this, tv,
							2 * 60 * 1000, 1000);
					timer.start();
				} else {
					if (jsonAprovado == 0) {
						new AlertDialog.Builder(TimerActivity.this)
								.setTitle("Aten��o")
								.setMessage("Aprova��o de cr�dito negada!")
								.setNeutralButton("OK", new OnClickListener() {
									@Override
									public void onClick(DialogInterface dialog,
											int which) {
										Intent intent = new Intent(
												TimerActivity.this,
												AreaClienteActivity.class);
										startActivity(intent);
										TimerActivity.this.finish();
									}
								}).show();
					} else {
						new AlertDialog.Builder(TimerActivity.this)
								.setTitle("Aten��o")
								.setMessage("Proposta Processada.")
								.setPositiveButton("Continuar",
										new OnClickListener() {
											@Override
											public void onClick(
													DialogInterface dialog,
													int which) {
												Intent intent = new Intent(
														TimerActivity.this,
														PropostaAprovada.class);
												startActivity(intent);
												TimerActivity.this.finish();
											}
										}).show();
					}
				}
			} else {
				if (semConexao == true) {
					new AlertDialog.Builder(TimerActivity.this)
							.setTitle("Aten��o")
							.setMessage("Sem conex�o de rede.")
							.setNeutralButton("OK", new OnClickListener() {
								@Override
								public void onClick(DialogInterface dialog,
										int which) {
									Intent intent = new Intent(
											TimerActivity.this,
											AreaClienteActivity.class);
									startActivity(intent);
									TimerActivity.this.finish();
								}
							}).show();
				} else {
					new AlertDialog.Builder(TimerActivity.this)
							.setTitle("Aten��o").setMessage(jsonMensagem)
							.setNeutralButton("OK", new OnClickListener() {
								@Override
								public void onClick(DialogInterface dialog,
										int which) {
									Intent intent = new Intent(
											TimerActivity.this,
											AreaClienteActivity.class);
									startActivity(intent);
									TimerActivity.this.finish();
								}
							}).show();
				}
			}
		}
	}
}
