package com.android.activity;

import com.android.functions.VariaveisGlobais;
import com.android.projetoavista1.R;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.TextView;

public class AreaClienteActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.activity_area_cliente);

		TextView txcliente = (TextView) findViewById(R.id.tvCliente);
		TextView txtacessar = (TextView) findViewById(R.id.tvacessarconta);
		TextView txtnsoucliente = (TextView) findViewById(R.id.tvnsCliente);
		TextView txtsolicite = (TextView) findViewById(R.id.tvSolicite);
		TextView txtlojista = (TextView) findViewById(R.id.tvlojista);
		TextView txtacessoconta = (TextView) findViewById(R.id.tvAcessoconta);
		
		VariaveisGlobais.mostrarMensagem = true;
		
		mudarFonte(txcliente, "fonts/Proxima_Nova_Regular.otf");
		mudarFonte(txtacessar, "fonts/Proxima_Nova_Regular.otf");
		mudarFonte(txtnsoucliente, "fonts/Proxima_Nova_Regular.otf");
		mudarFonte(txtsolicite, "fonts/Proxima_Nova_Regular.otf");
		mudarFonte(txtlojista, "fonts/Proxima_Nova_Regular.otf");
		mudarFonte(txtacessoconta, "fonts/Proxima_Nova_Regular.otf");

	}

	private void mudarFonte(TextView tv, String fonte) {
		Typeface type = Typeface.createFromAsset(getAssets(), fonte);
		tv.setTypeface(type);
	}

	public void onClickSelecionarOpcao(View view) {

		switch (view.getId()) {

		case R.id.lltSouCliente:
			new AlertDialog.Builder(AreaClienteActivity.this)
					.setTitle("Aten��o")
					.setMessage("M�dulo em desenvolvimento.")
					.setNeutralButton("OK", null).show();
			break;

		case R.id.lltNaoSouCliente:
			Intent souCliente = new Intent(getBaseContext(),
					CadastroCelularActivity.class);
			startActivity(souCliente);
			this.finish();
			break;

		case R.id.lltSouLojista:
			new AlertDialog.Builder(AreaClienteActivity.this)
					.setTitle("Aten��o")
					.setMessage("M�dulo em desenvolvimento.")
					.setNeutralButton("OK", null).show();
			break;
		}
	}

}
