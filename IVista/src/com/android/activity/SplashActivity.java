/*IVista Modulo não sou cliente
 * Desenvolvido por:
 * 		 Everaldo Cardoso de Araújo/Rodrigo Oliveira;
 * Cliente: 
 * 		 Avista Cartão de Crédito*/

package com.android.activity;

import com.android.projetoavista1.R;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.Window;

public class SplashActivity extends Activity {

	private final int duracaoTela = 2000;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.activity_splash);

		new Handler().postDelayed(new Runnable() {
			@Override
			public void run() {
				// VariaveisGlobais.tempoAprovacao=1;
				Intent acao = new Intent(SplashActivity.this,

				BuscaCepActivity.class);
				// TimerActivity.class);

				SplashActivity.this.startActivity(acao);
				SplashActivity.this.finish();
			}
		}, duracaoTela);
	}

}