package com.android.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.TextView;

import com.android.functions.VariaveisGlobais;
import com.android.projetoavista1.R;

public class ConfirmPrePagoActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.activity_ca_prepago);

		TextView txtEndCliente = (TextView) findViewById(R.id.txtEndCliente);
		TextView txtEndCliente2 = (TextView) findViewById(R.id.txtEndCliente2);

		txtEndCliente.setText(VariaveisGlobais.enderecoLogradouro + ", "
				+ VariaveisGlobais.enderecoNumero);
		txtEndCliente2.setText(VariaveisGlobais.enderecoBairro + ", "
				+ VariaveisGlobais.enderecoCidade + ", "
				+ VariaveisGlobais.enderecoUf);
	}

	public void onClickPrePagoBtnContinuar(View v) {
		Intent intent = new Intent(this, AreaClienteActivity.class);
		startActivity(intent);
		this.finish();

	}
}
