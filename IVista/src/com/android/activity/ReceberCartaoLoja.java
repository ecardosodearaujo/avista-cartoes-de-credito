package com.android.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.TextView;

import com.android.functions.VariaveisGlobais;
import com.android.projetoavista1.R;

public class ReceberCartaoLoja extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.activity_final_cartao);

		TextView txtCredito1 = (TextView) findViewById(R.id.txtCredito1);
		txtCredito1.setText("Compare�a na loja " + VariaveisGlobais.lojaNome
				+ " em at� 5 dias para imprimir o seu cartão "
				+ VariaveisGlobais.lojaTipoCartao
				+ ". Tenha em m�os o seu RG e CPF");
	}

	public void finalizaprocesso(View v) {
		ReceberCartaoLoja.this.finish();
		Intent intent = new Intent(this, AreaClienteActivity.class);
		startActivity(intent);
	}

}
