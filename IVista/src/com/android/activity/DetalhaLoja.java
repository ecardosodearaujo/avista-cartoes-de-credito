package com.android.activity;

import java.io.IOException;
import org.apache.http.client.ClientProtocolException;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.DialogInterface.OnClickListener;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.android.functions.GetSetJSON;
import com.android.functions.VariaveisGlobais;
import com.android.projetoavista1.R;

public class DetalhaLoja extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.activity_detalha_lojas);
		TextView txtNomeLoja = (TextView) findViewById(R.id.txtNomeLoja);
		TextView txtEndLoja = (TextView) findViewById(R.id.txtEndLoja);
		TextView txtTelLoja = (TextView) findViewById(R.id.txtTelLoja);

		txtNomeLoja.setText(VariaveisGlobais.lojaNome);
		txtEndLoja.setText(VariaveisGlobais.lojaEndereco);
		txtTelLoja.setText(VariaveisGlobais.lojaTelefone);

	}

	public void onClickBtnReceberEmCasa(View v) {
		Intent intent = new Intent(DetalhaLoja.this, CartaoPreAprovado.class);
		startActivity(intent);
		DetalhaLoja.this.finish();
	}

	public void onClickBtnConfirmar(View v) {

		switch (v.getId()) {
		case R.id.buttonReceberLoja:
			new AlertDialog.Builder(DetalhaLoja.this)
					.setTitle("Receber nesta Loja")
					.setMessage("Deseja continuar?")
					.setNegativeButton("N�o", new OnClickListener() {
						@Override
						public void onClick(DialogInterface dialog, int which) {
							dialog.dismiss();
						}
					}).setPositiveButton("Sim", new OnClickListener() {
						@Override
						public void onClick(DialogInterface dialog, int which) {
							new AlteraLoja().execute();
						}
					}).show();
		}
	}

	public void onClickFecharWindow(View v) {
		Intent intent = new Intent(DetalhaLoja.this, ListaDeLojasActivity.class);
		startActivity(intent);
		DetalhaLoja.this.finish();
	}

	public class AlteraLoja extends AsyncTask<Void, Void, Void> {
		String openUrl = "http://177.125.181.172/BossRest/nao/cliente/alterar/loja/";
		JSONObject jsonResposta1 = new JSONObject();
		JSONArray jsonResposta2 = new JSONArray();
		JSONObject jsonEnvio = new JSONObject();
		GetSetJSON getSetJson = new GetSetJSON();
		public ProgressDialog pDialog;
		int jsonRetorno = 1;
		String jsonMensagem;
		boolean semConexao = true;
		public ArrayAdapter<String> adapter;

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			pDialog = new ProgressDialog(DetalhaLoja.this);
			pDialog.setMessage("Aguarde um instante...");
			pDialog.setIndeterminate(false);
			pDialog.setCancelable(false);
			pDialog.show();
		}

		@Override
		protected Void doInBackground(Void... arg0) {
			try {
				jsonEnvio.put("PARAM_CODIGO_PROPOSTA", "6257443");
				jsonEnvio.put("PARAM_CODIGO_UNIDADE_CREDENCIADA",
						VariaveisGlobais.lojaCodigo);
				jsonResposta1 = getSetJson
						.enviarSolicitacao(openUrl, jsonEnvio);
				jsonRetorno = jsonResposta1.getInt("RETORNO");
				jsonMensagem = jsonResposta1.getString("MENSAGEM");
			} catch (JSONException e) {
				Log.i("JSONException", e.getMessage());
				jsonRetorno = 1;
			} catch (ClientProtocolException e) {
				Log.i("ClientProtocolException", e.getMessage());
				jsonRetorno = 1;
			} catch (IOException e) {
				Log.i("IOException", e.getMessage());
				semConexao = true;
				jsonRetorno = 1;
			}
			return null;
		}

		@Override
		protected void onPostExecute(Void Void) {
			pDialog.dismiss();
			if (jsonRetorno == 0) {
				Intent intent = new Intent(DetalhaLoja.this,
						ReceberCartaoLoja.class);
				startActivity(intent);
				DetalhaLoja.this.finish();
			} else {
				if (semConexao == false) {
					new AlertDialog.Builder(DetalhaLoja.this)
							.setTitle("Aten��o")
							.setMessage(jsonMensagem.toString())
							.setNeutralButton("ok", null).show();
				} else {
					new AlertDialog.Builder(DetalhaLoja.this)
							.setTitle("Aten��o")
							.setMessage("Sem conex�o de rede.")
							.setNeutralButton("ok", null).show();
				}
			}
		}
	}
}
