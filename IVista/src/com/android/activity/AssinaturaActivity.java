package com.android.activity;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import org.apache.http.client.ClientProtocolException;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.DialogInterface.OnClickListener;
import android.gesture.GestureOverlayView;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.functions.Base64;
import com.android.functions.GetSetJSON;
import com.android.functions.VariaveisGlobais;
import com.android.projetoavista1.R;

public class AssinaturaActivity extends Activity {

	ImageView iv;
	GestureOverlayView gv;
	TextView termodeuso;
	CheckBox c1;
	CheckBox c2;
	boolean imagSalva=false;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.activity_assinatura);

		c1 = (CheckBox) findViewById(R.id.checkBox1);
		c2 = (CheckBox) findViewById(R.id.checkBox2);

		gv = (GestureOverlayView) findViewById(R.id.layoutAssinatura);
	}

	public void onClickSelecionarOpcao(View view) {

		switch (view.getId()) {

		case R.id.tvSalvar:
			gv.setDrawingCacheEnabled(true);
			Bitmap b = Bitmap.createBitmap(gv.getDrawingCache());
			iv.setImageBitmap(b);

			break;

		case R.id.tvLimpar:
			apagar_assinatura(view); // setDrawingCacheEnabled(true);

			break;
		}

	}

	public void apagar_assinatura(View view) {
		gv.cancelClearAnimation();
		gv.clear(true);
		imagSalva=false;
	}

	public void saveSig(View view) {
		try {
			GestureOverlayView gestureView = (GestureOverlayView) findViewById(R.id.layoutAssinatura);
			gestureView.setDrawingCacheEnabled(true);
			Bitmap bm = Bitmap.createBitmap(gestureView.getDrawingCache());//Transforma a assinatura em imagem;
			//File f = new File(Environment.getExternalStorageDirectory()+ File.separator + "Assinatura.jpeg");
			//f.createNewFile();
			//FileOutputStream os = new FileOutputStream(f);
			//os = new FileOutputStream(f);
			ByteArrayOutputStream stream = new ByteArrayOutputStream();//Variavel que recebe a imagem em bytes;
			bm.compress(Bitmap.CompressFormat.JPEG, 100, stream);//Compressão da imagem para envio;
			byte[] byteArray = stream.toByteArray();//Transformado a imagem em array de bytes;
			VariaveisGlobais.imagemAssinaturaB64=Base64.encodeBytes(byteArray);//Guarda a imagem na variavel global;
			//os.close();
			Log.i("Imagem em base64: ", VariaveisGlobais.imagemAssinaturaB64);
			Toast.makeText(AssinaturaActivity.this,"Assinatura Salva Com Sucesso", Toast.LENGTH_SHORT).show();
			imagSalva=true;
		} catch (Exception e) {
			Log.v("Gestures", e.getMessage());
			e.printStackTrace();
		}
	}
	
	public void onClickBtnFinalizar(View v) {
		if (imagSalva==true){
			if (c1.isChecked() && c2.isChecked()) {
				if (VariaveisGlobais.aprovacaoTipoCartao==0){
					new EnviaFotos().execute();
				}else{
					Intent intentConfima = new Intent(AssinaturaActivity.this, ConfirmPrePagoActivity.class);
					startActivity(intentConfima);
					AssinaturaActivity.this.finish();
				}
			} else {
				new AlertDialog.Builder(AssinaturaActivity.this).setTitle("Importante").setMessage("Para prosseguir � necess�rio aceitar os termos.").setNeutralButton("OK", null).show();
			}
		}else{
			new AlertDialog.Builder(AssinaturaActivity.this).setTitle("Importante").setMessage("� necess�rio salvar a assinatura.").setNeutralButton("OK", null).show();
		}
	}

	public void onClickTermosDeUso(View v) {
		Intent intent = new Intent(this, TermosDeUsoActivity.class);
		startActivity(intent);
	}
	
	public void onClickTarifasDeUso(View v){
		Intent intent = new Intent(this, TarifasDeUsoActivity.class);
		startActivity(intent);
	}
	
	public void onClickFecharWindow(View v){
		VariaveisGlobais.voltarDadosPessoais=true;
		Intent intent = new Intent(AssinaturaActivity.this,MinhasFotosActivity.class);
		startActivity(intent);
		this.finish();
		
	}
	
	@Override
	public void onBackPressed(){
		dialogFechar();
	}
	
	public void onClickBtnCancelar(View v){
		dialogFechar();
	}
	
	public void dialogFechar(){
		new AlertDialog.Builder(AssinaturaActivity.this).setTitle("Deseja Cancelar?").setMessage("Ao cancelar o processo este n�mero de celular n�o poder� ser ultilizado em uma nova proposta.")
		.setNegativeButton("N�o", new OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				dialog.dismiss();
			}
		})
		.setPositiveButton("Sim", new OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				finish();
			}
		}).show();
	}
	
	public class EnviaFotos extends AsyncTask<Void,Void,Void>{
		public String openURL = "http://177.125.181.172/BossRest/nao/cliente/enviar/documentos/";
		public GetSetJSON getSetJson = new GetSetJSON();
		public JSONObject jsonResposta1;
		public JSONObject jsonEnvio = new JSONObject();
		public boolean semConexao = true;
		public int jsonRetorno = -1;
		public String jsonMensagem;
		public ProgressDialog pDialog;
		public int jsonTempo;
		
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			pDialog = new ProgressDialog(AssinaturaActivity.this);
			pDialog.setMessage("Aguarde um instante...");
			pDialog.setIndeterminate(false);
			pDialog.setCancelable(false);
			pDialog.show();
		}
		
		@Override
		protected Void doInBackground(Void... params) {
			try {
				semConexao=false;
				jsonEnvio.put("PARAM_CODIGO_PROPOSTA", VariaveisGlobais.aprovacaoCodigo);
				jsonEnvio.put("PARAM_IMAGEM_ASSINATURA", VariaveisGlobais.imagemAssinaturaB64);
				jsonEnvio.put("PARAM_IMAGEM_CLIENTE", VariaveisGlobais.imagemMinhaFotoB64);
				jsonEnvio.put("PARAM_IMAGEM_DOC_FRENTE", VariaveisGlobais.imagemDocFrontB64);
				jsonEnvio.put("PARAM_IMAGEM_DOC_VERSO", VariaveisGlobais.imagemDocEndB64);
				Log.i("JSON ENVIO:",jsonEnvio.toString());
				jsonResposta1=getSetJson.enviarSolicitacao(openURL, jsonEnvio);
				jsonRetorno=jsonResposta1.getInt("RETORNO");
				if (jsonRetorno==0){
					jsonTempo=jsonResposta1.getInt("TEMPO");
				}
				jsonMensagem=jsonResposta1.getString("MENSAGEM");
				
			} catch (JSONException e) {
				Log.i("JSONException ",e.getMessage());
			} catch (ClientProtocolException e) {
				Log.i("ClientProtocolException",e.getMessage());
			} catch (IOException e) {
				semConexao=true;
				Log.i("IOException",e.getMessage());
			}
			return null;
		}
		
		@Override
		protected void onPostExecute(Void Void) {
			pDialog.dismiss();
			if (jsonRetorno==0){
				new AlertDialog.Builder(AssinaturaActivity.this).setTitle("Aten��o").setMessage(jsonMensagem)
				.setPositiveButton("OK", new OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						if (VariaveisGlobais.aprovacaoTipoCartao==0){
							VariaveisGlobais.tempoAprovacao=jsonTempo;
							Intent intentTimer = new Intent(AssinaturaActivity.this,TimerActivity.class);
							startActivity(intentTimer);
						}/*else if (VariaveisGlobais.aprovacaoTipoCartao==1){
							Intent intentConfima = new Intent(AssinaturaActivity.this, ConfirmPrePagoActivity.class);
							startActivity(intentConfima);
						}*/
						AssinaturaActivity.this.finish();
					}
				}).show();
			}else{
				if (semConexao==false){
					new AlertDialog.Builder(AssinaturaActivity.this)
					.setTitle("Aten��o")
					.setMessage(jsonMensagem)
					.setNeutralButton("OK", null).show();
				}else{
					new AlertDialog.Builder(AssinaturaActivity.this)
					.setTitle("Aten��o")
					.setMessage("Sem conexão de rede.")
					.setNeutralButton("OK", null).show();
				}
			}
		}
	}
}
