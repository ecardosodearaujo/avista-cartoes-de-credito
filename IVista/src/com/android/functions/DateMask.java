package com.android.functions;

import android.text.Selection;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnKeyListener;
import android.widget.EditText;

public class DateMask implements OnKeyListener {

	@Override
	public boolean onKey(View v, int keyCode, KeyEvent event) {

		EditText ed = (EditText) v;

		if (event.getAction() == KeyEvent.ACTION_UP

		&& keyCode != KeyEvent.KEYCODE_DEL) {

			int length = ed.getText().toString().length();

			switch (length) {

			case 2: {

				ed.setTextKeepState(ed.getText() + "/");

				break;

			}

			case 5: {

				ed.setTextKeepState(ed.getText() + "/");

				break;

			}

			default:

				break;

			}
		}

		Selection.setSelection(ed.getText(), ed.getText().toString()

		.length());

		return false;

	}

}