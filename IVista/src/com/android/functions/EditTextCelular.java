package com.android.functions;

import android.content.Context;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.text.method.NumberKeyListener;
import android.util.AttributeSet;
import android.util.Log;
import android.widget.EditText;

public class EditTextCelular extends EditText {

	private boolean isUpdating;

	private int positioning[] = { 1, 2, 3, 6, 7, 8, 9, 10, 12, 13, 14, 15 };
	private int positioning8[] = { 1, 2, 3, 6, 7, 8, 9, 11, 12, 13, 14 };

	public EditTextCelular(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		initialize();
	}

	public EditTextCelular(Context context, AttributeSet attrs) {
		super(context, attrs);
		initialize();
	}

	public EditTextCelular(Context context) {
		super(context);
		initialize();
	}

	public String getCleanText() {
		String text = EditTextCelular.this.getText().toString();

		text.replaceAll("[^0-9]*", "");
		return text;
	}

	private void initialize() {

		final int maxNumberLength = 11;
		this.setKeyListener(keylistenerNumber);

		// this.setText("(  )      -    ");
		// this.setSelection(1);

		this.addTextChangedListener(new TextWatcher() {
			public void afterTextChanged(Editable s) {
				String current = s.toString();

				if (isUpdating) {
					isUpdating = false;
					return;
				}

				String number = current.replaceAll("[^0-9]*", "");
				if (number.length() > 11)
					number = number.substring(0, 11);
				int length = number.length();

				String paddedNumber = padNumber(number, maxNumberLength);

				String ddd = paddedNumber.substring(0, 2);
				String part1;
				String part2;
				if (ddd.equals("11") || ddd.equals("12") || ddd.equals("13")
						|| ddd.equals("14") || ddd.equals("15")
						|| ddd.equals("16") || ddd.equals("17")
						|| ddd.equals("18") || ddd.equals("19")
						|| ddd.equals("21") || ddd.equals("22")
						|| ddd.equals("24") || ddd.equals("27")
						|| ddd.equals("28") || ddd.equals("91")
						|| ddd.equals("93") || ddd.equals("94")
						|| ddd.equals("92") || ddd.equals("97")
						|| ddd.equals("95") || ddd.equals("96")
						|| ddd.equals("98") || ddd.equals("99")) {
					part1 = paddedNumber.substring(2, 7);
					part2 = paddedNumber.substring(7, 11);
				} else {
					part1 = paddedNumber.substring(2, 6);
					part2 = paddedNumber.substring(6, 10);
				}

				String phone = "(" + ddd + ") " + part1 + "-" + part2;

				isUpdating = true;
				EditTextCelular.this.setText(phone);

				if (ddd.equals("27")) {
					Log.d("#### NOVE 9", "##");
					EditTextCelular.this.setSelection(positioning[length]);
				} else {
					Log.d("#### OITO 8", "##");
					length = (length > 10) ? 10 : length;
					EditTextCelular.this.setSelection(positioning8[length]);

				}
			}

			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
			}

			public void onTextChanged(CharSequence s, int start, int before,
					int count) {
			}
		});
	}

	protected String padNumber(String number, int maxLength) {
		String padded = new String(number);
		for (int i = 0; i < maxLength - number.length(); i++)
			padded += " ";
		return padded;
	}

	private final KeylistenerNumber keylistenerNumber = new KeylistenerNumber();

	private class KeylistenerNumber extends NumberKeyListener {

		public int getInputType() {
			return InputType.TYPE_CLASS_NUMBER
					| InputType.TYPE_TEXT_FLAG_NO_SUGGESTIONS;
		}

		@Override
		protected char[] getAcceptedChars() {
			return new char[] { '0', '1', '2', '3', '4', '5', '6', '7', '8',
					'9' };
		}
	}
}