package com.android.functions;

import java.text.NumberFormat;

import com.android.projetoavista1.R;

import android.content.Context;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.widget.EditText;

public class EditTextMoeda extends EditText {

	private boolean isUpdating;
	private NumberFormat nf = NumberFormat.getCurrencyInstance();
	private EditText edtValorTotal;

	public EditTextMoeda(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		initialize();
	}

	public EditTextMoeda(Context context, AttributeSet attrs) {
		super(context, attrs);
		initialize();
	}

	public EditTextMoeda(Context context) {
		super(context);
		initialize();
	}

	public String getCleanText() {
		String text = EditTextMoeda.this.getText().toString();

		text.replaceAll("[^0-9]*", "");
		return text;
	}

	private void initialize() {
		
		edtValorTotal = (EditText) findViewById(R.id.edtDadosRenda);

		this.addTextChangedListener(new TextWatcher() {
			public void afterTextChanged(Editable s) {
			}

			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
			}

			public void onTextChanged(CharSequence s, int start, int before,
					int count) {

				if (isUpdating) {
					isUpdating = false;
					return;
				}

				isUpdating = true;
				String str = s.toString();
				// Verifica se j� existe a m�scara no texto.
				boolean hasMask = ((str.indexOf("R$") > -1 || str.indexOf("$") > -1) && (str
						.indexOf(".") > -1 || str.indexOf(",") > -1));
				// Verificamos se existe m�scara
				if (hasMask) {
					// Retiramos a m�scara.
					str = str.replaceAll("[R$]", "").replaceAll("[,]", "")
							.replaceAll("[.]", "");
				}

				try {
					// Transformamos o n�mero que est� escrito no EditText em
					// monet�rio.
					str = nf.format(Double.parseDouble(str) / 100);
					edtValorTotal.setText(str);
					edtValorTotal
							.setSelection(edtValorTotal.getText().length());
				} catch (NumberFormatException e) {
					s = "";
				}
			}

		});

	}

}