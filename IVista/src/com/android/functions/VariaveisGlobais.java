package com.android.functions;

import java.util.ArrayList;
import java.util.List;

public class VariaveisGlobais {

	// Variaveis a serem mandadas para aprovação de credito:
	public static String unidadeCredenciada="0";// Não se aplica;
	public static String codigoPromoter="0";// N�o se aplica;
	public static String contatoCelular;
	public static String contatoTelefone;
	public static String contatoCpf;
	public static String contatoNome;
	public static String contatoDataNas;
	public static String contatoEscolaridade;
	public static String contatoEstadoCivil;
	public static String contatoSexo;
	public static String contatoRendaMensal;
	public static String dadosAlterados ="false";
	public static String dadosVencimento;
	public static String enderecoCep;
	public static String enderecoUf;
	public static String enderecoCidade;
	public static String enderecoBairro;
	public static String enderecoLogradouro;
	public static String enderecoNumero;
	public static String enderecoComplemento;
	//Variaveis da Aprovação:
	public static int aprovacaoTipoCartao=-1;
	public static String aprovacaoTarifa="";
	public static String aprovacaoTermos="";
	public static String aprovacaoCodigo;
	//Variveis para envio das imagens:
	public static String imagemAssinaturaB64;
	public static String imagemMinhaFotoB64;
	public static String imagemDocFrontB64;
	public static String imagemDocEndB64;
	// Variaveis de Controle:
	public static List<String> jsonVencimento = new ArrayList<String>();
	public static boolean mostrarMensagem=true;
	public static boolean acabou=false;
	public static boolean voltarDadosPessoais=false;
	public static String lojaNome;
	public static String lojaCodigo;
	public static String lojaEndereco;
	public static String lojaTelefone;
	public static String lojaTipoCartao;
	public static boolean enderecoBuscaCep=false;
	public static boolean enderecoJaInstanciada=false;
	public static boolean enderecoMostraMensagemEndereco=true;
	public static int tempoAprovacao=1;
}
