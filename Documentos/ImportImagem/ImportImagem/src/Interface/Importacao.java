/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Interface;

import BancoDados.databaseAcess;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.sql.Blob;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import javax.swing.ImageIcon;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;


//@author everaldo

public class Importacao extends javax.swing.JFrame {
    public databaseAcess con = new databaseAcess();
    public String local=null;
  
    
    public Importacao() {
        initComponents();
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        edtCaminhoImg = new javax.swing.JTextField();
        btnEscolherImg = new javax.swing.JButton();
        btnImportarImgem = new javax.swing.JButton();
        jLabel2 = new javax.swing.JLabel();
        edtCodProduto = new javax.swing.JTextField();
        btnEscolherProduto = new javax.swing.JButton();
        edtNomeProduto = new javax.swing.JTextField();
        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        btnVerImagemBD = new javax.swing.JButton();
        jLabel3 = new javax.swing.JLabel();
        btnImportCaminho = new javax.swing.JButton();
        btnImagemCaminho = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setResizable(false);

        edtCaminhoImg.setEditable(false);

        btnEscolherImg.setText("Escolher Imagem");
        btnEscolherImg.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnEscolherImgActionPerformed(evt);
            }
        });

        btnImportarImgem.setText("Importar Imagem");
        btnImportarImgem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnImportarImgemActionPerformed(evt);
            }
        });

        jLabel2.setText("Caminho da Imagem:");

        edtCodProduto.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                edtCodProdutoFocusLost(evt);
            }
        });

        btnEscolherProduto.setText("...");
        btnEscolherProduto.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnEscolherProdutoActionPerformed(evt);
            }
        });

        edtNomeProduto.setEditable(false);

        jPanel1.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        jLabel1.setFont(new java.awt.Font("Ubuntu", 1, 14)); // NOI18N
        jLabel1.setText("Importação de Imagem:");

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(119, 119, 119)
                .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGap(113, 113, 113))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
        );

        btnVerImagemBD.setText("Ver Imagem BD");
        btnVerImagemBD.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnVerImagemBDActionPerformed(evt);
            }
        });

        jLabel3.setText("Produto:");

        btnImportCaminho.setText("Importar Caminho");
        btnImportCaminho.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnImportCaminhoActionPerformed(evt);
            }
        });

        btnImagemCaminho.setText("Ver Imagem Caminho");
        btnImagemCaminho.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnImagemCaminhoActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(edtCodProduto, javax.swing.GroupLayout.PREFERRED_SIZE, 49, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnEscolherProduto, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(edtNomeProduto))
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel2)
                            .addComponent(jLabel3))
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addComponent(edtCaminhoImg)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnEscolherImg)))
                .addContainerGap())
            .addGroup(layout.createSequentialGroup()
                .addGap(60, 60, 60)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(btnImportarImgem, javax.swing.GroupLayout.DEFAULT_SIZE, 127, Short.MAX_VALUE)
                    .addComponent(btnVerImagemBD, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(btnImportCaminho, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(btnImagemCaminho, javax.swing.GroupLayout.DEFAULT_SIZE, 120, Short.MAX_VALUE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jLabel3)
                .addGap(7, 7, 7)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(edtNomeProduto, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(edtCodProduto)
                    .addComponent(btnEscolherProduto))
                .addGap(18, 18, 18)
                .addComponent(jLabel2)
                .addGap(3, 3, 3)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(edtCaminhoImg, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnEscolherImg))
                .addGap(28, 28, 28)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnImportCaminho)
                    .addComponent(btnImportarImgem))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnVerImagemBD)
                    .addComponent(btnImagemCaminho))
                .addGap(24, 24, 24))
        );

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents
    //Busca do produto via assistente:
    private void btnEscolherProdutoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnEscolherProdutoActionPerformed
        String parametros = "CODPRODUTO AS 'CODIGO', PRODUTO AS 'DESCRICÃO'";
        Pesquisar pesquisa = new Pesquisar(null,true,parametros,"TAB_PRODUTOS");
        pesquisa.comboPesquisa.addItem("CodProduto");
        pesquisa.comboPesquisa.addItem("Produto");
        pesquisa.setVisible(true);
        edtCodProduto.setText(pesquisa.dados.get(0));
        edtNomeProduto.setText(pesquisa.dados.get(2));
    }//GEN-LAST:event_btnEscolherProdutoActionPerformed
    //Busca do produto via focus:
    private void edtCodProdutoFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_edtCodProdutoFocusLost
        try {
            con.OpenConect();
            con.Select("SELECT PRODUTO FROM TAB_PRODUTOS WHERE CODPRODUTO='"+edtCodProduto.getText()+"'");
            con.rs.first();
            edtNomeProduto.setText(con.rs.getString("PRODUTO"));
        } catch (SQLException ex) {
            edtCodProduto.setText("");
            edtNomeProduto.setText("");
        }
        con.CloseConect();
    }//GEN-LAST:event_edtCodProdutoFocusLost
    //Escolha da imagem:
    private void btnEscolherImgActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnEscolherImgActionPerformed
        if(!edtCodProduto.getText().equals("")){
            JFileChooser fileChooser = new JFileChooser();  
            fileChooser.setDialogTitle("Importar imagem");  
            fileChooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
            if (fileChooser.showOpenDialog(this)==JFileChooser.APPROVE_OPTION){
                local = fileChooser.getSelectedFile().getPath();
                edtCaminhoImg.setText(local);
            }
        }else{
            JOptionPane.showMessageDialog(null, "Informe Codigo do produto", "Atenção",0);
        } 
    }//GEN-LAST:event_btnEscolherImgActionPerformed
    //Importar imagem:
    private void btnImportarImgemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnImportarImgemActionPerformed
        if(edtCodProduto.getText().equals("")){
            JOptionPane.showMessageDialog(null, "Informe Codigo do produto", "Atenção",0);
        }else if (edtCaminhoImg.getText().equals("")){ 
            JOptionPane.showMessageDialog(null, "Informe O caminho da imagem", "Atenção",0);
        }else{
            try{
                con.OpenConect();
                File arquivo = new File(local);
                FileInputStream inputStream = new FileInputStream(arquivo);
                PreparedStatement pstmt  = con.getConnection().prepareStatement("UPDATE TAB_PRODUTOS SET FOTO=? WHERE CODPRODUTO=?");
                pstmt.setBinaryStream(1, inputStream,(int)arquivo.length());
                pstmt.setString(2, edtCodProduto.getText());
                pstmt.execute();
                JOptionPane.showMessageDialog(null, "Imagem importada com sucesso!", "Atenção",1);
            }catch(IOException | SQLException ex){
                   JOptionPane.showMessageDialog(null, "Não foi possivel salvar a imagem: "+ex.getMessage(), "Atenção",0);
            }
            con.CloseConect();
        }
    }//GEN-LAST:event_btnImportarImgemActionPerformed
    //Visualizar imagem:
    private void btnVerImagemBDActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnVerImagemBDActionPerformed
        if(!edtCodProduto.getText().equals("")){
            try {
                con.OpenConect();
                con.Select("SELECT FOTO FROM TAB_PRODUTOS WHERE CODPRODUTO="+edtCodProduto.getText());
                if (con.rs.next()){
                    Blob blob = con.rs.getBlob("FOTO");
                    VerImagem vImagem = new VerImagem();
                    ImageIcon imageIcon = new ImageIcon(blob.getBytes(1, (int)blob.length()));
                    vImagem.lblImagem.setIcon(imageIcon);
                    vImagem.setVisible(true);   
                }
            } catch (SQLException ex) {
                System.out.println("Erro: "+ex.getMessage());
            }
        }else{
            JOptionPane.showMessageDialog(null, "Informe Codigo do produto", "Atenção",0);
        }
        con.CloseConect();
    }//GEN-LAST:event_btnVerImagemBDActionPerformed

    private void btnImportCaminhoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnImportCaminhoActionPerformed
        if(edtCodProduto.getText().equals("")){
            JOptionPane.showMessageDialog(null, "Informe Codigo do produto", "Atenção",0);
        }else if (edtCaminhoImg.getText().equals("")){
            JOptionPane.showMessageDialog(null, "Informe o Caminho da imagem", "Atenção",0);
        }else{
            con.OpenConect();
            con.Update("UPDATE TAB_PRODUTOS SET FOTOCAMINHO='"+local+"' WHERE CODPRODUTO="+edtCodProduto.getText());
            con.CloseConect();
            JOptionPane.showMessageDialog(null, "Caminho gravado com sucesso!", "Atenção",1);

        }
    }//GEN-LAST:event_btnImportCaminhoActionPerformed

    private void btnImagemCaminhoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnImagemCaminhoActionPerformed
        if(!edtCodProduto.getText().equals("")){
            try{
                con.OpenConect();
                con.Select("SELECT FOTOCAMINHO FROM TAB_PRODUTOS WHERE CODPRODUTO="+edtCodProduto.getText());
                con.rs.beforeFirst();
                while(con.rs.next()){
                    VerImagem vImagem = new VerImagem();
                    vImagem.lblImagem.setIcon(new ImageIcon(con.rs.getString("FOTOCAMINHO"))); 
                    vImagem.setVisible(true);
                }
            }catch(SQLException e){
                System.out.println("Erro:"+e.getMessage());
            }
        }else{
            JOptionPane.showMessageDialog(null, "Informe Codigo do produto", "Atenção",0);
        }
        con.CloseConect();
    }//GEN-LAST:event_btnImagemCaminhoActionPerformed

    public static void main(String args[]) {
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Importacao.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Importacao.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Importacao.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Importacao.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Importacao().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnEscolherImg;
    private javax.swing.JButton btnEscolherProduto;
    private javax.swing.JButton btnImagemCaminho;
    private javax.swing.JButton btnImportCaminho;
    private javax.swing.JButton btnImportarImgem;
    private javax.swing.JButton btnVerImagemBD;
    private javax.swing.JTextField edtCaminhoImg;
    private javax.swing.JTextField edtCodProduto;
    private javax.swing.JTextField edtNomeProduto;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JPanel jPanel1;
    // End of variables declaration//GEN-END:variables
}
