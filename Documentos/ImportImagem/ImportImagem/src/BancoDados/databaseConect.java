package BancoDados;

/*Importações:*/
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

//@author Everaldo C. Araújo;

public class databaseConect{
	/*Declaração das variaveis para acesso ao banco de dados:*/
	public String driver="com.mysql.jdbc.Driver";//Driver de conexeção do banco de dados;
	public String banco="jdbc:mysql://127.0.0.1:3306/CCCDBD";//Caminho do banco de dados;
	public String usuario="root";//Usuário do banco de dados;
	public String senha="M1n3Rv@7";//Senha do Banco de dados;
	
	/*Declaração de metodo da variavel connection:*/
	/*--------------------------------------------------*/
	private Connection connection=null;//Declarando a váriavel connection;
	public Connection getConnection(){//Método get da váriavel connection; 
		return connection;
	}
	/*Declaração de metodo da variavel statement:*/
	/*--------------------------------------------------*/
	public static Statement statement=null;//Declarando a váriavelstametent; 
	public Statement getStatement(){//M�todo get da váriavel stametent;
		return statement;
	}
	/*Método que abre a conexão com o banco:*/
	/*--------------------------------------------------*/
	public boolean OpenConect(){
		boolean abriu=true;//Variavel de status;
	  	try{
	        Class.forName(driver);//Determinando o drive do banco de dados;
	        connection=DriverManager.getConnection(banco,usuario,senha);//String de autenticacao no banco;
	        statement=connection.createStatement();//Criando o statement que será usado mais tarde;
	        System.out.println("Conexão Aberta!");//Mensagem de log;
	    }catch (Exception e){
	    	System.out.println("A conexão com o banco de dados falhou!");//Mensagem de log;
                abriu=false;
            }
            return abriu;//Retorno do metodo;
	}
	/*Método que Fecha a Conexão com o Banco de dados:*/
	/*--------------------------------------------------*/
	public boolean CloseConect(){
		boolean fechou=true;//Variavel de status;
		try {
			connection.close();//Fechando a variavel que mantem a conexão aberta;
			System.out.println("Conexão Fechada!");//Mensagem de log;
			}catch (SQLException ex) {
				System.out.println("Falha para fechar a conexão!");//Mesangem de log;
				fechou=false;
				}
		return fechou;//Retorno do metodo;
	}
}
