package BancoDados;

/*Importações*/
import com.mysql.jdbc.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;

//@author Everaldo C. Araújo;

public class databaseAcess extends databaseConect{
	public ResultSet rs = null;//Carrega o resultado do Select;
	public ResultSetMetaData rsmd=null;//Recebe o numero de linhas da consulta;
	
        /*Método para fazer Select no banco de dados:*/ 
	/*------------------------------------------------------------------------------------*/   
    public void Select(String query){ //Recebe como parametro uma String SQL;
    	try {
            System.out.println(query.trim());//Imprime a query para verificação;
            rs=getStatement().executeQuery(query.trim());//O resultSet recebe o resultado do select;
            rsmd=rs.getMetaData();
        } catch (SQLException ex) {
            System.out.println("A conexão com o banco de dados falhou!");//Aviso em caso de erro;
        }
    }
	/*Método que faz Insert no banco de dados:*/
    /*------------------------------------------------------------------------------------*/
    public boolean Insert(String query){//Recebe uma String SQL como parametro;
    	boolean salvou=true;
        OpenConect();//Abre a conexão com o banco;
    	try {
    		System.out.println(query.trim());//Imprime a query para verificação;
            PreparedStatement pst = (PreparedStatement) getConnection().prepareStatement(query.trim());//Prepara o Insert;
            pst.executeUpdate(query.trim());//Executando o update no banco;
        } catch (SQLException ex) {
            System.out.println("Não foi possivel Salvar os dados!");//Alerta em caso de erro;
            salvou=false;
        }
    	CloseConect();//Fecha a conexão com banco;
    	return salvou;//Retorno do metodo;
    }
    /*Metodo para Excluir os dados de dados:*/
    /*------------------------------------------------------------------------------------*/
    public boolean Delete(String query){//Recebe como parâmetro uma String SQL; 
        boolean deletou=true;
    	OpenConect();//Abrindo a conexão com o banco;
    	try { 
    		System.out.println(query.trim());//Imprime a query para verificação;
            getStatement().executeUpdate(query.trim());//Execulta a instrução;                
        } catch (SQLException ex) {
        	System.out.println("Não foi possivel Excluir os dados!");//Alerta em caso de erro;
        	deletou=false;
        }
    	CloseConect();//Fecha a conexão com o banco;
    	return deletou;//Retorno do metodo;
    }
    /*Método para Atualizar os dados de dados:*/
    /*------------------------------------------------------------------------------------*/
    public boolean Update(String query){//Recebe uma String SQL como paramentro;
      boolean atualizou=true;
      OpenConect();//Abrindo a conexão com banco;
      try {
    	  System.out.println(query.trim());//Imprime a query para verificação;
    	  getStatement().execute(query.trim());//Execultando o Update;
      } catch (SQLException ex) {
    	  System.out.println("Não foi possivel atualizar os dados");//Alerta em caso de erro;
    	  atualizou=false;
      }
      CloseConect();//Fecha a conexão com o banco;
      return atualizou;//Retorno do metodo;
    }
}